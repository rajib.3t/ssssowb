<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomTraits\SeoMetaElements;
use App\CustomTraits\MediaTrait;
class Category extends Model
{
    use SeoMetaElements,MediaTrait;
    protected $fillable = ['name','parent','slug','status','content','image_id','mobile_image_id'];

    /**
     * Get The owning posts
     */
    public function posts()
    {
        return $this->morphedByMany('App\Post', 'post_category_relationships');
    }

}
