<?php
namespace App\CustomTraits;
use App\MetaElement;
trait SeoMetaElements
{
     /**
     * Get The owning meta element
     */

    public function meta_element()
    {
        return $this->morphOne('App\MetaElement','metaelementable');
    }
}