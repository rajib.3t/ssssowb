<?php
namespace App\CustomTraits;
trait MediaTrait
{
    public function media()
    {
        return $this->morphToMany('App\Media', 'mediaable')->withPivot(['alt_text']);
    }
}