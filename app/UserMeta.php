<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $fillable = ['meta_name','meta_value'];

    public function usermetable(){
        return $this->morphTo(__FUNCTION__, 'model_type', 'model_id');
    }

    
}
