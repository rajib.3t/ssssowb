<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $fillable = ['menu_id','model_id','model_type','menu_type','label','menu_order','custom_url','css_class','parent','structure_type'];


    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

    /**
     * Get The owning model
     */
    public function model()
    {
        return $this->morphTo('model');
    }

    public function normalChildren()
    {
        return $this->hasMany(self::class, 'parent')->orderby('menu_order','ASC')->with('model');
    }

    public function getParent()
    {
        return $this->belongsTo(self::class, 'parent')->with('model');
    }

}
