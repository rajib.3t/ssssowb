<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomTraits\SeoMetaElements;
use App\CustomTraits\MediaTrait;
use Kyslik\ColumnSortable\Sortable;
class Post extends Model
{
    use SeoMetaElements,MediaTrait,Sortable;
    protected $fillable = [
        'title',
        'postable_id',
        'postable_type',
        'post_type',
        'content',
        'slug',
        'image_id',
        'mobile_banner_id',
        'template',
        'parent',
        'menu_order',
        'status'
    ];
    public $sortable = ['title','status'];
    /**
     * Get the owning postable model.
     */
    public function postable()
    {
        return $this->morphTo();
    }

    /**
     * Get The owning Categories
     */
     public function category()
     {
        return $this->morphToMany('App\Category', 'post_category_relationships');
     }

    /**
     * Get The owning Categories
     */
    public function tags()
    {
       return $this->morphToMany('App\Tag', 'post_tag_relationships');
    }
}
