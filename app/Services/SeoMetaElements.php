<?php
namespace App\Services;
use App\MetaElement;
class SeoMetaElements
{

    protected $emItems = array(
                        'seo_title',
                        'description',
                        'keywords',
                        'robots',
                        'revisit_after',
                        'og_locale',
                        'og_type',
                        'og_image',
                        'og_title',
                        'og_url',
                        'og_description',
                        'og_site_name',
                        'author',
                        'canonical',
                        'geo_region',
                        'geo_placename',
                        'geo_position',
                        'ICBM'
                    );

    public function getElements($model)
    {

        $em = $model->meta_element()->first();
        //dd($em->attributes);


        $str = '';
        foreach($this->emItems as $key => $item)
        {
            $str .= '<div class="form-group">
            <label for="site_name">'.ucwords(str_replace('_', ' ', $item)).'</label>';
            if($item =='description' || $item =='keywords' || $item =='og_description'){
                $str.='<textarea id="'.$item.'" placeholder="'.ucwords(str_replace('_', ' ', $item)).'" class="form-control" name="'.$item.'" >'.$em->$item.'</textarea>';
            }else{
                $str .='<input id="'.$item.'" placeholder="'.ucwords(str_replace('_', ' ', $item)).'" class="form-control" name="'.$item.'" type="text" value="'.$em->$item.'">';
            }

            $str .='</div>';
        }
       return $str;


    }

    public function updateElement($model,$request)
    {

        $inputs = [];
        foreach($this->emItems as $key => $item )
        {
            $inputs[$item]=$request->$item;
        }
        $model->meta_element()->update($inputs);
    }



    public function getMeta($model){
        $meta = $model->meta_element()->first();
        if($meta->title){

            $meta_info = '<title>'. $meta->title . '</title>'.PHP_EOL;
        }
        if($meta->description){

            $meta_info .= '<meta name="description" content="'. $meta->description .'" />'.PHP_EOL;
        }
        if($meta->keywords){

            $meta_info .= '<meta name="keywords" content="'. $meta->keywords .'" />'.PHP_EOL;

        }
        if($meta->robots){

            $meta_info .= '<meta name="robots" content="'. $meta->robots .'" />'.PHP_EOL;

        }
        if($meta->revisit_after){

            $meta_info .= '<meta name="revisit-after" content="'. $meta->revisit_after .'" />'.PHP_EOL;

        }
        if($meta->og_locale){

            $meta_info .= '<meta property="og:locale" content="'. $meta->og_locale .'" />'.PHP_EOL;

        }
        if($meta->og_type){

            $meta_info .= '<meta property="og:type" content="'. $meta->og_type .'" />'.PHP_EOL;

        }
        if($meta->og_image){

            $meta_info .= '<meta property="og:image" content="'. $meta->og_image .'" />'.PHP_EOL;
        }
        if($meta->og_title){

            $meta_info .= '<meta property="og:title" content="'. $meta->og_title .'" />'.PHP_EOL;

        }
        if($meta->og_url == NULL){

          $uri = $_SERVER['REQUEST_URI'];
          $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
          $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

          $meta_info .= '<meta property="og:url" content="'. $url .'" />'.PHP_EOL;
        }
        if($meta->og_description){

            $meta_info .= '<meta property="og:description" content="'. $meta->og_description .'" />'.PHP_EOL;
        }
        if($meta->og_site_name){

            $meta_info .= '<meta property="og:site_name" content="'. $meta->og_site_name .'" />'.PHP_EOL;

        }
        if($meta->author){

            $meta_info .= '<meta name="author" content="'. $meta->author .'" />'.PHP_EOL;
        }
        if($meta->canonical == NULL){

          $uri = $_SERVER['REQUEST_URI'];
          $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
          $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

          $meta_info .= '<link rel="canonical" href="'. $url .'" />'.PHP_EOL;
        }
        if($meta->geo_region){

            $meta_info .= '<meta name="geo.region" content="'. $meta->geo_region .'" />'.PHP_EOL;
        }
        if($meta->geo_placename){

            $meta_info .= '<meta name="geo.placename" content="'. $meta->geo_placename .'" />'.PHP_EOL;
        }
        if($meta->geo_position){

            $meta_info .= '<meta name="geo.position" content="'. $meta->geo_position .'" />'.PHP_EOL;
        }
        if($meta->ICBM){

            $meta_info .= '<meta name="ICBM" content="'. $meta->ICBM .'" />';
        }

        return $meta_info;
    }
}
