<?php
namespace App\Services;
use App\Post;
use App\Category;
use Str;
use App\Tag;
class Slug
{
    
    public function createCategorySlug($title, $parent = 0,$id = 0)
    {
        // Normalize the title
        
        $slug = Str::slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getCategoryRelatedSlugs($slug, $parent,$id,);
        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug', $slug))
        {
            return $slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) 
        {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) 
            {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');

    }

    protected function getCategoryRelatedSlugs($slug, $parent, $id)
    {
        return Category::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->where('parent',$parent)
            ->get();
    }

    public function createPageSlug($title,$id = 0, $parent = 0)
    {
        // Normalize the title
        
        $slug = Str::slug($title);

        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getPageRelatedSlugs($slug, $id, $parent);
        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug', $slug))
        {
            return $slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) 
        {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) 
            {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');

    }

    protected function getPageRelatedSlugs($slug, $id = 0, $parent=0)
    {
        return Post::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->where('parent',$parent)
            ->get();
    }

    public  function getPageParent($id,$type='page')
    {

        $post = Post::where([
            ['post_type','=',$type],
            ['id','<>',$id]
            ])->get();


        $p = array(0=>'No Parent');
        foreach ($post as $key => $value) 
        {
            $p[$value->id]=$value->title;
        }
        return $p;
    }

    public  function createPageUrl($id,$top = false){
        if($top){
            
            $slug = $this->createPageUrl($id);
            $slugArr = explode('/',$slug);
            array_pop($slugArr);
            if(empty($slugArr)){
                return '';
            }
            return implode('/',$slugArr);
        }else{
            $str = $this->getRelatedPageSlug($id);
        
            $slugArr = explode('/',$str);
            $revSlugArr = array_reverse(array_filter($slugArr));
            return implode('/',$revSlugArr);
        }
        
    }

    protected function getRelatedPageSlug($id){
        $slug = '';
        $p = Post::find($id);
        $slug .=$p->slug."/";
        $parent = $p->parent;
        if($parent != 0){
            $slug .=$this->getRelatedPageSlug($parent);
        }
        
        return $slug;
    }
}