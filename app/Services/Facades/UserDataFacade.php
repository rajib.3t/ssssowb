<?php

namespace App\Services\Facades;

use Illuminate\Support\Facades\Facade;

class UserDataFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'Userdata'; }
}