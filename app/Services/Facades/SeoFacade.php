<?php

namespace App\Services\Facades;

use Illuminate\Support\Facades\Facade;

class SeoFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'Seo'; }
}
