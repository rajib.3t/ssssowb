<?php
namespace App\Services;
class UserData
{

    public function get($model,$meta_name,$all = false)
    {
        if($model->meta_details()->where(['meta_name'=>$meta_name])->first()){
        if($all)
        {
            return $model->meta_details()->where(['meta_name'=>$meta_name])->first();
        }
        else
        {
            return $model->meta_details()->where(['meta_name'=>$meta_name])->first()->meta_value;
        }
    }else{
        return false;
    }
        
    }

    public function update($model,$meta_name,$meta_value)
    {
        $usermeta = $model->meta_details()->where(['meta_name'=>$meta_name])->first();
        if($usermeta)
        {
            $res = $usermeta->update(['meta_value'=>$meta_value]);
            if($res)
            {
                return $usermeta;
            }
        }
        else
        {
           return  $model->meta_details()->create(['meta_name'=>$meta_name,'meta_value'=>$meta_value]);
        }

        return false;
    }
}