<?php
namespace App\Services;
use App\Media;
class Upload{
    public function go_upload($name,$model=null,$attach=null,$other = false){
        $media = Media::all();

        //$all = '<div class="row mg-files" data-sort-destination="" data-sort-id="media-gallery">';

        //$all .='</div>';
        $url = $id = $alt  ='';

        $el_tag ='<a class=" modal-sizes btn btn-success" href="#modal_'.$name.'"><i class="fa fa-upload" aria-hidden="true"></i></a>';
        if($model){
            if($attach){
                if($other)
                {
                    $r =$model->media()->find($attach);
                }else
                {
                    $r =$model->media()->find($model->$attach);
                }

            }else{
                $r =$model->media()->first();
            }

            if($r){
                $url = $r->url;
                $id = $r->id;
                $alt = $r->pivot->alt_text;

                $el_tag ='<a class="btn btn-danger" onclick="remove_url(this)" hrel="#" data-push="'.$name.'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            }
        }

        $str = '<div class="input-group mb-md">
                    <input type="text" name="'.$name.'" class="form-control" value="'.$url.'">
                    <input type="hidden" name="'.$name.'_id" class="form-control" value="'.$id.'">
                        <span class="input-group-btn upload-btn-'.$name.' ">
                        '.$el_tag.'

                        </span>

                </div>
                <input type="text" name="'.$name.'_alt" class="form-control" placeholder="Alt text..." value="'.$alt.'">';
        $str .='<div id="modal_'.$name.'" class="modal-block modal-block-full mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">File Upload</h2>
                <button class="btn btn-default modal-dismiss">Cancel</button>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                <div class="tabs">
                <ul class="nav nav-tabs">
                    <li id="li-nav-form-'.$name.'" class="active">
                        <a href="#upload-'.$name.'" data-toggle="tab">Upload</a>
                    </li>
                    <li id="li-nav-select-'.$name.'">
                        <a href="#select-'.$name.'" data-toggle="tab" data-elename ="'.$name.'" onclick="load_image(this)">Select</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="upload-'.$name.'" class="tab-pane active">
                    <form class="form-horizontal form-bordered">
                    <div class="form-group">
                                                <label class="col-md-3 control-label">File Upload</label>
                                                <input type="hidden" name="token-'.$name.'" value="'.csrf_token().'">
												<div class="col-md-6">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input">
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
																<span class="fileupload-exists">Change</span>
																<span class="fileupload-new">Select file</span>
																<input type="file" name="upload-file-'.$name.'">
															</span>
															<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
                                                </div>
                                                <div class="col-md-3">
                                                <button class="btn btn-primary" data-push="'.$name.'" onclick="common_upload(this)" >Submit</button>
                                                </div>
                                            </div>


                    </div>
                    <div id="select-'.$name.'" class="tab-pane">
                    <input type="hidden" class="page-no" id="page-no-'.$name.'" value="1">
                    <div class="row mg-files"  id="popupload" data-sort-destination="" data-sort-id="media-gallery">

                    </div>
                    <div class="ajax-load text-center" style="display:none">
	                        <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More media</p>
                    </div>
                    <div class="form-group">

                        <input type="button" data-name="'.$name.'" class="mb-xs mt-xs mr-xs btn btn-primary loadmore" value="Load More">
                    </div>
                    </div>
                </div>
            </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary modal-confirm">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>';

       return $str;
    }
}
