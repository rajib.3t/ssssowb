<?php

use App\Option;
use App\MenuItem;
function active_tab($active_tab,$tab_opt)
{
	return (in_array( $active_tab , $tab_opt) ) ? 'nav-active nav-expanded' : '';
}

function active_tab_child($active_tab,$tab_opt)
{
	return ($active_tab == $tab_opt ) ? 'nav-active' : '';
}

function makeFlat($menu_id, $arr,$parent=0)
     {

		$result = array();
		foreach($arr as $key => $row) {

			$r['menu_id']=$menu_id;
			$r['parent']=$parent;
			$r['menu_order']=$key;
			$r['model_id']=$row['item_id'];
			$r['model_type']=$row['item_model'];
			$r['menu_type']=$row['menu_type'];
			$r['label']=$row['label'];
			$r['css_class']=$row['css'];
			$r['custom_url']=$row['url'];
			$result = MenuItem::create($r);
			$row['parent']=$result->id;
			if(isset($row['children']) && count($row['children']) > 0) {
				$result = makeFlat($menu_id,$row['children'],$row['parent']);

			}


		}
		return $result;


}

function adminmenu($items)
{
	$tree = '';
	$i =0;

	foreach($items as $item)
	{


		$tree.='<li class="dd-item dd3-item" data-url="'.$item->custom_url.'" data-menu_type="'.$item->menu_type.'" data-id="'.$item->id.'" data-item_id="'.$item->model_id.'" data-item_model = "'.$item->model_type.'"data-label="'.$item->label.'" data-css="'.$item->css_class.'">';
		$tree .='<div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="pull-left">'.$item->label.'</div><div class="pull-right">'.$item->menu_type.' | <a href="javascript:void(0)" onclick="edit_menu_item(this)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>| <a href="javascript:void(0)" data-action="removeRow" onclick="deleteItem(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div>';
		if($item->normalChildren())
		{
			$tree .='<ol class="dd-list">';
			$tree .= adminmenu($item->normalChildren()->get());
			$tree .='</ol>';
			$i++;
		}
		$tree.='</li>';

		$i++;
	}
	return $tree;
}

function getMenuItems($items,$parent=0)
{

}


