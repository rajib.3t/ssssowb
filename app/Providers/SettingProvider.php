<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;
use Config;
use Setting;
use DB;
use Illuminate\Support\Facades\Schema;
class SettingProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('Setting',function(){
            return new App\Services\Setting;
        });

        App::bind('Upload',function(){
            return new App\Services\Upload;
        });

        App::bind('Slug',function(){
            return new App\Services\Slug;
        });

        App::bind('Seo',function(){
            return new App\Services\SeoMetaElements;
        });

        App::bind('Userdata',function(){
            return new App\Services\UserData;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        if( Schema::hasTable('options')){
                if(Setting::get('site_name')){
                    Config::set('app.name', Setting::get('site_name')); 
                }
                if(Setting::get('timezone')){
                    Config::set('app.timezone', Setting::get('timezone')); 
                }

                if(Setting::get('site_lang')){
                    Config::set('app.locale', Setting::get('site_lang')); 
                }
        }
    }
}
