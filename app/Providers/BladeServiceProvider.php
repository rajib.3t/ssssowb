<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        Blade::directive('permit', function ($expression) {
            $condition = false;
             list($guard, $permission) = explode(',', $expression);


            return "<?php if (Auth::guard({$guard})->user()->hasPermission({$guard},{$permission})) { ?>";

        });

        Blade::directive('endpermit', function () {

            return "<?php } ?>";

        });
        Blade::directive('permitgroup', function ($expression) {

            $condition = true;
            $r  = explode(',', $expression);

            return "<?php if({$condition}){

                 ?>";

        });

        Blade::directive('endpermitgroup', function () {

            return "<?php } ?>";

        });

    }
}
