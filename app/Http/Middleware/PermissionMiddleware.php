<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\UnauthorizedException;
class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }
        $guard = auth()->guard(); 
        $sessionName = $guard->getName(); 
        $parts = explode("_", $sessionName);
        unset($parts[count($parts)-1]);
        unset($parts[0]);
        $guardName = implode("_",$parts);
        
        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);
        
            foreach ($permissions as $permission) {
                if (Auth::guard($guardName)->user()->hasPermission($guardName,$permission)) {
                    return $next($request);
                }
            }
            throw UnauthorizedException::forPermissions($permissions);
    }
}
