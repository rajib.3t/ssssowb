<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;
class RolePermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        

        $guard_arr = config('auth.guards');
        $guards = array_keys($guard_arr);
        $guards = implode(",",$guards);
        $routeName = Route::currentRouteName();
        switch($routeName)
        {

            case 'admin.role.save':
                {
                    return [ 
                        'name'  => 'required|string|max:100|unique:roles,name,null,null,guard_name,'.$this->guard,
                        'guard' => 'required|string|in:'.$guards
                        
                    ];
                }
            case 'admin.role.update':
                {
                    return [

                        'name'  => 'required|unique:roles,name,'.$this->id.',id,guard_name,'.$this->guard,
                        'guard' => 'required|string|in:'.$guards
    
                    ];
                }
            case 'admin.permission.save':
                {
                    return [
                    
                        'name'  => 'required|string|max:100|unique:permissions,name,null,null,guard_name,'.$this->guard,
                        'guard' => 'required|string|in:'.$guards,
                        'description' => 'required|string'
                    ];
                }
            case 'admin.permission.update':
                {
                    return [
                    
                        'name'  => 'required|unique:permissions,name,'.$this->id.',id,guard_name,'.$this->guard,
                        'guard' => 'required|string|in:'.$guards,
                        'description' => 'required|string'
                    ];
                }
        }
    }
}
