<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;



class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = Route::currentRouteName();
        switch($route){
            case 'admin.setting.general.save':
                {
                    return[
                        'site_name'=>'required'
                    ];
                }
            case 'admin.setting.underconstruction.save':
                {
                    return [

                    ];  
                }
            case 'admin.setting.miscellaneous.save':
                {
                    return [
                        'row_in_admin_list'=>'required|numeric|min:1'
                    ];
                }
                default:{
                    return [

                    ];
                }
        }
        
    }

    public function messages()
    {
        return [
            'row_in_admin_list.min'=>'Must be Positive and greater then "0"'
        ];
    }
}
