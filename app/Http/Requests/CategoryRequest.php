<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = Route::currentRouteName();
        
        switch($route){
            case 'admin.categories.save':
                {
                    if($this->ajax())
                    {
                        return [];
                    }
                    
                        return[
                            'name'=>'required'
                        ];
                    
                    
                }
            case 'admin.categories.update':
                {
                    return[
                        'name'=>'required'
                    ];
                }
            default:{
                return [

                ];
            }
        }
    }
}
