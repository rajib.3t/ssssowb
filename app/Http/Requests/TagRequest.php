<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = Route::currentRouteName();

        switch($route){
            case 'admin.tags.save':
                {
                    return[
                        'name'=>'required|regex:/^[\pL\s\-]+$/u|unique:tags,name'
                    ];
                }
            default:{
                return [

                ];
            }
        }
    }

    public function messages()
    {
        return[
            'name.regex' => 'Please enter a valid name',
            'name.unique' => 'This tag name is already exists',
        ];
    }
}
