<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use App\Category;
use App\Tag;
class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categories = Category::where(['status'=>1])->get();
        $all_cats =[];
        foreach($categories  as $key => $cat)
        {
            $all_cats[$cat->id] = $cat->id;
        }
        $all_category = implode(',',$all_cats);
        
        $tags = Tag::where(['status'=>1])->get();
        $all_tags =[];
        foreach($tags  as $key => $tag)
        {
            $all_tags[$tag->id] = $tag->id;
        }
        $all_tags_str = implode(',',$all_cats);
        $route = Route::currentRouteName();
        switch($route){
            case 'admin.posts.save':
                {
                    return[
                        'title'=>'required',
                        'categories'=>'required|array|in:'.$all_category
                    ];
                }
            case 'admin.posts.update':
                {
                    return[
                        'title'=>'required',
                        'categories'=>'required|array|in:'.$all_category,
                        'tag'=>'in:'.$all_tags_str
                    ];  
                }
            default:{
                return [

                ];
            }
        }
    }
}
