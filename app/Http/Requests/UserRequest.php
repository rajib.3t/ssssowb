<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = Route::currentRouteName();
        $method = $this->method();
        $type = $routeName.'.'.$method;
        switch($type)
        {
            case 'admin.users.save.POST':
                {

                        return [
                            'name' => 'required|regex:/^[\pL\s\-]+$/u',
                            'email' => 'required|email|unique:users,email',
                            'new_password' => 'required',
                            'new_password' => 'required|string|min:8|same:conf_password',
                            'conf_password' => 'required',
                            'roles'=>'required|array'
                        ];

                }
                case 'admin.users.update.POST':
                    {
                        if($this->new_password !=''){
                            return [
                                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                                'email' => 'required|email|unique:users,email,'.$this->id,
                                'new_password' => 'required',
                                'new_password' => 'required|string|min:8|same:conf_password',
                                'conf_password' => 'required',
                                'roles'=>'required|array'
                               ];

                        }else{
                            return [
                                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                                'email' => 'required|email|unique:users,email,'.$this->id,
                                'roles'=>'required|array'
                               ];
                        }
                    }
        }
    }
}
