<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Category;
use Slug;
use Seo;
use DB;
use Validator;
class CategoryController extends Controller
{
    private $parent=array();

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->parent[0]='No Parent';
        $parent_cat = Category::where(['status'=>1])->get();

        foreach($parent_cat  as $key => $cat)
        {
            $this->parent[$cat->id] = $cat->name;
        }


        $this->middleware('auth:admin');
        $this->middleware('permission:list.category',['only'=>['index']]);
        $this->middleware('permission:store.category',['only'=>'store']);
        $this->middleware('permission:edit.category',['only'=>'edit']);
        $this->middleware('permission:update.category',['only'=>'update']);
        $this->middleware('permission:delete.category',['only'=>'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $parent = $this->parent;
        $categories = Category::all();
        return view('admin.categories.list',compact('parent','categories'));
    }

    /**
     * Show the form for creating a new resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {

        
        if($request->ajax())
        {
            $validator = Validator::make($request->all(), [
                'name'=>'required'
            ]);
            
            if($validator->fails())
            {
                
                    return response()->json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                
                    ), 200); // 400 being the HTTP code for an invalid request.
                
            }
            $inputs['name']=$request->name;
            $inputs['parent']=$request->parent;
            $inputs['status']=1;
            $inputs['slug']=Slug::createCategorySlug($request->name,$request->parent);
            $result = Category::create($inputs);
            $result->meta_element()->create(['seo_title'=>$request->name]);
            if($result)
            {
                return response()->json(array('status'=>true,'id'=>$result->id,'name'=>$result->name));
            }
            else
            {
                return response()->json(array('id'=>$result->id,'name'=>$result->name));
            }
        }
        else
        {
            $inputs['name']=$request->name;
            $inputs['parent']=$request->parent;
            $inputs['status']=1;
            $inputs['slug']=Slug::createCategorySlug($request->name,$request->parent);

            $result = Category::create($inputs);
            $result->meta_element()->create(['seo_title'=>$request->name]);
            if($result)
            {
                return redirect()->route('admin.categories')
                        ->with('success','Category created successfully.');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parent = $this->parent;

        unset($parent[$id]);
        $category = Category::find($id);
        if($category)
        {
            $remove = Category::find($id)->pluck('name', 'id')->toArray();

            return view('admin.categories.edit',compact('category','parent'));
        }
        else
        {
            return redirect()->route('admin.categories')
                    ->with('error',$id. ' category id is not exist');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {

        $category= Category::find($id);
        $inputs['name']=$request->name;
        $inputs['parent']=$request->parent;
        $inputs['status']=$request->status;
        $inputs['content']=$request->content;
        if($category->parent != $request->parent)
        {
            $inputs['slug']=Slug::createCategorySlug($request->name,$request->parent,$id);
        }


        $result = $category->update($inputs);

        if($result)
        {
            Seo::updateElement($category ,$request);
            return redirect()->route('admin.categories.edit',$id)
                    ->with('success','Category edited successfully.');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Category::find($id);
        $cat->posts()->detach();
        $cat->meta_element()->delete();
        $cat->media()->detach();
        $result = Category::destroy($id);
        if($result )
        {
            return redirect()->route('admin.categories')
                            ->with('success','Category deleted successfully.');
        }
    }
}
