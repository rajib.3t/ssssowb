<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RolePermissionRequest;
use App\Permission;
class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:list.permission',['only'=>['index']]);
        $this->middleware('permission:create.permission',['only'=>['store']]);
        $this->middleware('permission:edit.permission',['only'=>['edit']]);
        $this->middleware('permission:update.permission',['only'=>['update']]);
        $this->middleware('permission:addrole.topermission',['only'=>['addPermissionToRoles']]);
        $this->middleware('permission:delete.permission',['only'=>['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.permissions.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolePermissionRequest $request)
    {
        $result = Permission::create([
            'name'=>$request->name,
            'guard_name'=>$request->guard,
            'description'=>$request->description
        ]);
        if($result)
        {
            return redirect()->route('admin.permissions')
                    ->with('success','Permissions added successfully.');
        }
        else
        {
            return redirect()->route('admin.permissions')
                    ->with('error','Something are wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);
        if($permission)
        {
            return view('admin.permissions.edit',compact('permission'));
        }
        else
        {
            return redirect()->route('admin.permissions')
                    ->with('error',$id .' id dose not exist!');   
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolePermissionRequest $request, $id)
    {
        $permission = Permission::find($id);
        $result = $permission->update(
            [
                'name'=>$request->name,
                'guard_name'=>$request->guard,
                'description'=>$request->description
            ]
            );

        if($result)
        {
            return redirect()->route('admin.permission.edit',$id)
                    ->with('success','Permission updated successfully.');   
        }
        else
        {
            return redirect()->route('admin.permission.edit',$id)
                    ->with('error','Something are wrong!');
        }
    }
    /**
     * Add Role to Permission
     */
    public function addPermissionToRoles(Request $request, $id)
    {
        $permission = Permission::find($id);
        if($request->roles)
        {
            $result = $permission->roles()->sync($request->roles);
            if($result)
            {
                return redirect()->route('admin.permission.edit',$id)
                        ->with('success','Role added to Permission successfully.');   
            }
            else
            {
                return redirect()->route('admin.permission.edit',$id)
                        ->with('error','Something are wrong!'); 
            }
        }
        else
        {
            $result = $permission->roles()->detach();
            if($result)
            {
                return redirect()->route('admin.permission.edit',$id)
                        ->with('success','All Roles remove from Permisstion successfully.');   
            }
            else
            {
                return redirect()->route('admin.permission.edit',$id)
                        ->with('error','Something are wrong!'); 
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::find($id);
        if($permission)
        {
            
            $permission->roles()->detach();
           
            $result = Permission::destroy($id);
            if($result)
            {
                return redirect()->route('admin.permissions')
                        ->with('success','Permissions deleted successfully.');
            }
            else
            {
                return redirect()->route('admin.permissions')
                    ->with('error','Somethings are wrong!');
            }
            
        }
        else
        {
            return redirect()->route('admin.permissions')
                    ->with('error',$id .' id dose not exist!'); 
        }

    }
}
