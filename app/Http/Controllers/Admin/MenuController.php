<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MenuRequest;
use App\Menu;
use App\Post;
use App\Category;
use App\Tag;
use App\MenuItem;
class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:list.menu',['only'=>['index']]);
        $this->middleware('permission:change.menu.status',['only'=>['status']]);
        $this->middleware('permission:store.menu',['only'=>['store']]);
        $this->middleware('permission:edit.menu',['only'=>['edit']]);
        $this->middleware('permission:update.menu',['only'=>'update']);
        $this->middleware('permission:add.menu.item',['only'=>'addItem']);
        $this->middleware('permission:store.menu.item',['only'=>'saveMenuItems']);
        $this->middleware('permission:delete.menu',['only'=>['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();
        return view('admin.menus.list',compact('menus'));
    }

    /**
     * Change Status of menu
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status($id)
    {
        $menu = Menu::find($id);

        if($menu->status==1)
        {
            $res = $menu->update(['status'=>0]);
        }
        else
        {
            $res = $menu->update(['status'=>1]);
        }

        if($res)
        {
            return redirect()->route('admin.menus')
                        ->with('success','Menu status changes successfully.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $inputs['name']=$request->name;
        $inputs['status']=1;
        $result = Menu::create($inputs);
        if($result)
        {
            return redirect()->route('admin.menus')
                        ->with('success','Menu created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        if($menu)
        {
            return view('admin.menus.edit',compact('menu'));
        }
        else
        {
            return redirect()->route('admin.menus')
                        ->with('error',$id .' menu id does not exist!');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        $menu = Menu::find($id);
        $inputs['name']=$request->name;
        $res = $menu->update($inputs);
        if($res)
        {
            return redirect()->route('admin.menus')
                        ->with('success','Menu edited successfully.');
        }

    }

    /**
     * Add Items to Specific menu
     * @param  int  $id
     */
    public function addItem($id)
    {
        $menu = Menu::find($id);
        $savemenu = $menu->menu_items()->where(['parent'=>0])->orderby('menu_order','ASC')->get();

        if($menu)
        {
            $allPages = Post::select('id','title')->where(['post_type'=>'page','status'=>1])->get();
            $pages=[];
            foreach($allPages as $key =>$page)
            {
                $pages[$page->id]=$page->title;
            }
            $posts = [];
            $allPosts = Post::select('id','title')->where(['post_type'=>'post','status'=>1])->get();
            foreach($allPosts as $key =>$post)
            {
                $posts[$post->id]=$post->title;
            }
            $allCategories = Category::where(['status'=>1])->get();
            $categories = [];
            foreach($allCategories as $key =>$cat)
            {
                $categories[$cat->id]=$cat->name;
            }
            $tags =[];
            $allTags = Tag::where(['status'=>1])->get();
            foreach($allTags as $key =>$tag)
            {
                $tags[$tag->id]=$tag->name;
            }
            return view('admin.menus.additems',compact('menu','pages','posts','categories','tags'));
        }
        else
        {
            return redirect()->route('admin.menus')
                        ->with('error',$id .' menu id does not exist!');
        }

    }

    /**
     * Get typewise menu details
     */

     public function getMenuDetails(Request $request)
     {
         $type = $request->type;
         $res = [];
         $ids = $request->ids;
         if($request->ajax())
         {
             switch($type)
             {
                 case 'page':
                    $models = Post::find($ids);
                    foreach($models as $key =>$item)
                    {
                        $res[$key]['id']=$item->id;
                        $res[$key]['name']=$item->title;
                        $res[$key]['model']=get_class($item);

                    }
                break;
                case 'post':
                    $models = Post::find($ids);
                    foreach($models as $key =>$item)
                    {
                        $res[$key]['id']=$item->id;
                        $res[$key]['name']=$item->title;
                        $res[$key]['model']=get_class($item);

                    }
                break;
                case 'category':
                    $models = Category::find($ids);
                    foreach($models as $key =>$item)
                    {
                        $res[$key]['id']=$item->id;
                        $res[$key]['name']=$item->name;
                        $res[$key]['model']=get_class($item);

                    }
                break;
                case 'tag':
                    $models = Tag::find($ids);
                    foreach($models as $key =>$item)
                    {
                        $res[$key]['id']=$item->id;
                        $res[$key]['name']=$item->name;
                        $res[$key]['model']=get_class($item);

                    }
                break;

             }


             return response()->json($res);
         }
     }

     public function saveMenuItems(Request $request)
     {

         if($request->ajax())
         {

            $menus= $request->menu;
            $id = $request->menu_id;
            //$menuObj = Menu::find($id);
            //$res = $menuObj->update(['menu_data'=>json_encode($menus)]);


             $deletedRows = MenuItem::where('menu_id', $id)->delete();
             $inputs=makeFlat($id,$menus);
            if($inputs)
            {
                return response()->json(array('status'=>true));
            }
         }
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        if($menu)
        {
            $menu->menu_items()->delete();
            $result = Menu::destroy($id);
            if($result)
            {
                return redirect()->route('admin.menus')
                        ->with('success','Menu Deleted successfully!');
            }
            else
            {
                return redirect()->route('admin.menus')
                        ->with('error','Something is wrong!');
            }
        }
        else
        {
            return redirect()->route('admin.menus')
                        ->with('error',$id .' menu id does not exist!');
        }
    }
}
