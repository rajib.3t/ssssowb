<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AdminProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use App\Role;
use App\Permission;
class AdminUsers extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:list.adminuser',['only'=>['index']]);
        $this->middleware('permission:create.adminuser',['only'=>['create']]);
        $this->middleware('permission:store.adminuser',['only'=>'store']);
        $this->middleware('permission:edit.adminuser',['only'=>'edit']);
        $this->middleware('permission:update.adminuser',['only'=>'update']);
        $this->middleware('permission:delete.adminuser',['only'=>'destroy']);
        $this->middleware('permission:adminuser.special.permission.save',['only'=>['specialPermission']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Admin::where('id','!=',Auth::guard('admin')->user()->id)->get();
        return view('admin.adminuser.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('guard_name','admin')->pluck('name','id')->toArray();
        return view('admin.adminuser.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminProfile $request)
    {
        $user = Admin::create([
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => Hash::make($request->new_password)
        ]);
        if($user)
        {
            $user->roles()->sync($request->roles);
            return redirect()->route('admin.adminusers')
                    ->with('success', 'Admin User Created Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = Admin::find($id);
        $admin = Auth::guard('admin')->user();
        $roles = Role::where('guard_name','admin')->pluck('name','id')->toArray();
        if($user && $id != Auth::guard('admin')->user()->id){
            $userRoles = $user->roles()->get();
            $per = [];
            foreach($userRoles as $role)
            {
                $per = array_merge($per,$role->permissions()->pluck('name','id')->toArray());
            }
            $allPermissions = Permission::where('guard_name','admin')->pluck('name','id')->toArray();
            $resOfPermissions = array_diff($allPermissions,$per);
            
            return view('admin.adminuser.edit',compact('user','roles','resOfPermissions'));
        }
        elseif($id ==Auth::guard('admin')->user()->id)
        {
            return redirect()->route('admin.profile');

        }
        else
        {
            return redirect()->route('admin.adminusers')
                        ->with('error',$id .' id dose not exist!');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminProfile $request, $id)
    {
        $user = Admin::find($id);
        $inputs['name']=$request->name;
        $inputs['email']=$request->email;
        if($request->new_password !='')
        {
            $inputs['password'] = Hash::make($request->new_password);
        }

        $result = $user->update($inputs);
        $user->roles()->sync($request->roles);
            if($result){
                return redirect()->route('admin.adminuser.edit',$id)
                        ->with('success','Admin User updated successfully.');
            }
            else
            {
                return redirect()->route('admin.adminuser.edit',$id)
                        ->with('error','Somethings is wrong!');
            }
    }

    /**
     * To Give Special Permission To the Admin User
     */
    public function specialPermission(Request $request, $id)
    {
        $user = Admin::find($id);
        $result = $user->permissions()->sync($request->permissions);
        if($result){
            return redirect()->route('admin.adminuser.edit',$id)
                    ->with('success','Assing special permission.');
        }
        else
        {
            return redirect()->route('admin.adminuser.edit',$id)
                    ->with('error','Somethings is wrong!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
