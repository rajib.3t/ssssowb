<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Media;
use Illuminate\Support\Facades\Cache;
class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:list.media',['only'=>['index']]);
        $this->middleware('permission:create.media',['only'=>'create']);
        $this->middleware('permission:store.media',['only'=>'store']);
        $this->middleware('permission:store.media.editor',['only'=>'ckditor_store']);
        $this->middleware('permission:browse.media.editor',['only'=>['ckbrowse']]);
        $this->middleware('permission:delete.media.all',['only'=>['deleteall']]);
        $this->middleware('permission:delete.media',['only'=>['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        
        $media = Cache::remember('page-' . $request->page, 10, function(){
            return Media::paginate(3);
        });
       if($request->ajax())
       {
           
            $name = $request->name;
           if(!empty($request->page))
           {
            $view = view('admin.media.data',compact('media','name'))->render();
                return response()->json(['html'=>$view]);
           }
           else
           {
            return view('admin.media.listpopup',compact('media','name'));
           }


       }
       else
       {

        return view('admin.media.index',compact('media'));
       }

    }

    public function loadmore(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $route = $request->route()->getName();
        if($request->ajax())
        {
            if($request->hasFile('file'))
            {
                $file = $request->file('file');
                //dd($file->getClientMimeType() );
                $filenamewithextension = $file->getClientOriginalName();
                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $alt = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $file->getClientOriginalExtension();

                //filename to store
                $f = $filename.'_'.time();
                $filenametostore = $f.'.'.$extension;

                //Upload File
                $file->storeAs('public/uploads', $filenametostore);
                $url = asset('storage/uploads/'.$filenametostore);
                $attach = array(
                    'name'=>$f,
                    'filename'=>$filenametostore,
                    'url'=>$url,
                    'alt'=>$alt,
                    'minetype'=>$file->getClientMimeType(),
                    'size'=>$file->getSize()
                );
                $media = Media::create($attach);
                return response()->json($media);
            }
        }
        else
        {

                        $file = $request->file;
                        //dd($file->getClientMimeType() );
                        $filenamewithextension = $file->getClientOriginalName();
                        //get filename without extension
                        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                        $alt = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                        //get file extension
                        $extension = $file->getClientOriginalExtension();

                        //filename to store
                        $f = $filename.'_'.time();
                        $filenametostore = $f.'.'.$extension;

                        //Upload File
                        $file->storeAs('public/uploads', $filenametostore);
                        $url = asset('storage/uploads/'.$filenametostore);
                        $attach = array(
                            'name'=>$f,
                            'filename'=>$filenametostore,
                            'url'=>$url,
                            'alt'=>$alt,
                            'minetype'=>$file->getClientMimeType(),
                            'size'=>$file->getSize()
                        );
                        $media = Media::create($attach);
                        return $media;





        }

    }

    /**
     * Ckeditor File Upload
     */
    public function ckditor_store(Request $request){
           // dd($request);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $file = $request->file('upload');

            //dd($file->getClientMimeType() );
            $filenamewithextension = $file->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $alt = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $file->getClientOriginalExtension();

            //filename to store
            $f = $filename.'_'.time();
            $filenametostore = $f.'.'.$extension;

            //Upload File
            $file->storeAs('public/uploads', $filenametostore);
            $url = asset('storage/uploads/'.$filenametostore);
            $attach = array(
                'name'=>$f,
                'filename'=>$filenametostore,
                'url'=>$url,
                'alt'=>$alt,
                'minetype'=>$file->getClientMimeType(),
                'size'=>$file->getSize()
            );
            $media = Media::create($attach);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$media->url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8');
            echo $re;

    }
    /**
     * CkEditor Browse Image
     */
    public function ckbrowse(Request $request){
        //$media = Media::all();
        $media = Cache::rememberForever('key', function () {
            return  Media::all();
        });
        return view('admin.media.browse',compact('media'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Delete All
     */
    public function deleteall(Request $request)
    {
        if($request->ajax())
        {
            foreach($request->ids  as $id)
            {
                $media = Media::find($id);
                $media->posts()->detach();
                $media->option()->detach();
                $media->admin()->detach();
                $media->categories()->detach();
            }

            $result = Media::destroy($request->ids);
            if($result )
            {
                 return response()->json(array('status'=>true));
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::find($id);
        $media->posts()->detach();
        $media->option()->detach();
        $media->admin()->detach();
        $media->categories()->detach();

        $result = Media::destroy($id);
        if($result )
        {
            return redirect()->route('admin.media')
                            ->with('success','Media deleted successfully.');
        }
    }
}
