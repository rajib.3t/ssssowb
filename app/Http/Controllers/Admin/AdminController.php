<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AdminProfile;
use Userdata;
use Hash;
use App\Role;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index()
    {
        return view('admin.dashboard');
    }

    public function profile(AdminProfile $request)
    {
        $admin = Auth::guard('admin')->user();
        $roles = Role::where('guard_name','admin')->pluck('name','id')->toArray();
        if($request->isMethod('PATCH'))
        {

            $inputs['name']=$request->name;
            $inputs['email']=$request->email;

            if($request->new_password !='')
            {
                $inputs['password'] = Hash::make($request->new_password);
            }
            if($request->profile_image !='')
            {
                $profile_image = Userdata::update($admin,'profile_image',$request->profile_image_id);
                $admin->media()->sync([$profile_image->meta_value=>['alt_text'=>$request->profile_image_alt]]);
            }
            else
            {
                $profile_image = Userdata::update($admin,'profile_image','');
                $admin->media()->detach($request->profile_image_id);
            }

            $result = $admin->update($inputs);

            if($result)
            {
                return redirect()->route('admin.profile')
                        ->with('success','Profile updated successfully.');
            }
        }



        return view('admin.profile',compact('admin','roles'));
    }
}
