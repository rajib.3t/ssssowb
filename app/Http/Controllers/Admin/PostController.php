<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Category;
use App\Tag;
use App\Post;
use Slug;
use Seo;
use Setting;

class PostController extends Controller
{
    protected $categories =array();
    protected $tags=[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $categories = Category::where(['status'=>1])->get();

        foreach($categories  as $key => $cat)
        {
            $this->categories[$cat->id] = $cat->name;
        }
        $tags = Tag::where(['status'=>1])->get();
        foreach($tags  as $key => $tag)
        {
            $this->tags[$tag->id] = $tag->name;
        }
        $this->middleware('auth:admin');
        $this->middleware('permission:list.post',['only'=>['index']]);
        $this->middleware('permission:create.post',['only'=>['create']]);
        $this->middleware('permission:store.post',['only'=>['store']]);
        $this->middleware('permission:edit.post',['only'=>['edit']]);
        $this->middleware('permission:update.post',['only'=>'update']);
        $this->middleware('permission:delete.post',['only'=>['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::sortable()->where('post_type', 'post')->orderBy('created_at', 'desc')->paginate(Setting::get('row_in_admin_list'));
        return view('admin.posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->categories;
        return view('admin.posts.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {


        $inputs['title'] = $request->title;
        $inputs['postable_id']=Auth::guard('admin')->user()->id;
        $inputs['postable_type'] = get_class(Auth::guard('admin')->user());
        $inputs['post_type']='post';
        $inputs['slug']=Slug::createPageSlug($request->title);
        $inputs['template']='posts_templates.single';
        $inputs['status']=1;
        $inputs['parent']=0;
        $result = Post::create($inputs);
        $result->meta_element()->create(['seo_title'=>$request->title]);
        if($request->categories != null)
        {
            $result->category()->sync($request->categories);
        }
        if($result){
            return redirect()->route('admin.posts.edit',$result->id)
                    ->with('success','Post created successfully.');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post= Post::find($id);
        if($post)
        {
            $categories = $this->categories;
            $tags = $this->tags;
            $parent_categories = array_merge(array(0=>'No Parent'),$categories);
            return view('admin.posts.edit',compact('post','categories','parent_categories','tags'));
        }
        else
        {
            return redirect()->route('admin.posts')
                    ->with('error',$id .' post id is not exist');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {

        $post = Post::find($id);
        $inputs['title']= $request->title;
        $parent = $request->parent;
        if($post->parent != $parent)
        {
            $inputs['slug']=Slug::createPageSlug($request->title,$id,$parent);
            $inputs['parent']=$parent;
        }
        $inputs['content'] = $request->content;
         if($request->image  != null && $request->image_id != null)
         {
            $inputs['image_id']=$request->image_id;
            //$banner_image = Post::find($id);
            $post->media()->sync([$request->image_id=>['alt_text'=>$request->image_alt]],false);
            //dd($a);

        }
        else if($request->image_id != null)
        {
            $inputs['image_id']='';
            //$banner_image = Post::find($id);
            $post->media()->detach($request->image_id);
        }

        if($request->modile_banner_image  != null && $request->modile_banner_image_id != null)
        {
            $inputs['mobile_banner_id']=$request->modile_banner_image_id;
            //$modile_banner_image = Post::find($id);
            $post->media()->sync([$request->modile_banner_image_id=>['alt_text'=>$request->modile_banner_image_alt]],false);
        }
        else if($request->modile_banner_image_id != null)
        {
            $inputs['mobile_banner_id']='';
            //$modile_banner_image = Post::find($id);
            $post->media()->detach($request->modile_banner_image_id);
        }

        $inputs['status']=$request->status;

        $result = $post->update($inputs);
        if($result){
            $post->category()->sync($request->categories);
            $post->tags()->sync($request->tags);
            Seo::updateElement($post ,$request);
            return redirect()->route('admin.posts.edit',$id)
                    ->with('success','Post edited successfully.');
        }
    }


     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->media()->detach();
        $post->category()->detach();
        $post->tags()->detach();
        $post->meta_element()->delete();
        $result = Post::destroy($id);
        if($result)
        {
            return redirect()->route('admin.posts')
                    ->with('success','Post deleted successfully.');
        }

    }
}
