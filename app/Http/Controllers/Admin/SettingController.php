<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SettingRequest;
use Setting;
use App\Option;
class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:setting.general',['only'=>['general']]);
        $this->middleware('permission:setting.underConstruction',['only'=>['underConstruction']]);
        $this->middleware('permission:setting.save',['only'=>'save']);
    }
    /**
     * Site General Website
     */
    public function general()
    {
        return view('admin.setting.general');
    }
    /**
     * Under Construction
     */
    public function underConstruction()
    {
        return view('admin.setting.underconstruction');
    }
    /**
     * Miscellaneouse Setting
     */
    public function miscellaneous()
    {
        return view('admin.setting.miscellaneous');
    }
    // Save Option Routewise
    public function save(SettingRequest $request)
    {

        $route = $request->route()->getName();
        switch($route){
            case 'admin.setting.general.save':
                $site_name = $request->site_name;
                Setting::update('site_name',$site_name);
                if($request->site_tag !=''){
                    Setting::update('tag_line',$request->site_tag);
                }

                Setting::update('site_lang',$request->language );
                Setting::update('timezone',$request->timezone );
                if($request->site_logo !=''){
                    $logo_id = Setting::update('site_logo_id',$request->site_logo_id );
                    $logo_image = Option::find($logo_id);
                    $logo_image->media()->sync([$request->site_logo_id=>['alt_text'=>$request->site_logo_alt]],false);

                }else{
                    $logo_id = Setting::update('site_logo_id','');
                    $logo_image = Option::find($logo_id);
                    $logo_image->media()->detach($request->site_logo_id );
                }
                if($request->site_favicon  != ''){
                    $favicon_id = Setting::update('site_favicon_id',$request->site_favicon_id );
                    $favicon_image = Option::find($favicon_id);
                    $favicon_image->media()->sync([$request->site_favicon_id=>['alt_text'=>$request->site_favicon_alt]],false);
                }else{
                    $favicon_id = Setting::update('site_favicon_id','');
                    $favicon_image = Option::find($favicon_id);
                    $favicon_image->media()->detach($request->site_favicon_id);
                }
                return redirect()->route('admin.setting.general')
                ->with('success','General setting  successfully Updated.');
            case 'admin.setting.underconstruction.save':
                if($request->switch !=''){
                    Setting::update('under_construction',$request->switch);
                }else{
                    Setting::update('under_construction','');
                }
                return redirect()->route('admin.setting.underconstruction')
                ->with('success','Under Construction setting  successfully Updated.');
            case 'admin.setting.miscellaneous.save':
                $row_in_admin_list = $request->row_in_admin_list;
                Setting::update('row_in_admin_list',$row_in_admin_list);
                return redirect()->route('admin.setting.miscellaneous')
                ->with('success','Setting  successfully Updated.');
        }
    }
}
