<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Tag;
use Slug;
use Str;
use Seo;
class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:list.tag',['only'=>['index']]);
        $this->middleware('permission:store.tag',['only'=>'store']);
        $this->middleware('permission:store.tag.ajax',['only'=>'ajax']);
        $this->middleware('permission:change.tag.status',['only'=>'status']);
        $this->middleware('permission:delete.tag',['only'=>'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();
        return view('admin.tags.list',compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        $inputs['name']=$request->name;
        $inputs['slug']=Str::slug($request->name);
        $inputs['status']=1;
        $result = Tag::create($inputs);
        if($result)
        {
            $result->meta_element()->create(['seo_title'=>$request->name]);
            return redirect()->route('admin.tags')
                        ->with('success','Tag created successfully.');
        }
    }

    /**
     * Create Tag through AJAX
     */
    public function ajax(Request $request)
    {
        if($request->ajax())
        {
            $tag = Tag::where(['name'=>$request->tag])->first();

            if($tag)
            {
                return response()->json(array('id'=>$tag->id,'name'=>$tag->name));
            }
            else
            {
                $inputs['name']=$request->tag;
                $inputs['slug']=Str::slug($request->tag);
                $inputs['status']=1;
                $result = Tag::create($inputs);
                if($result)
                {
                    $result->meta_element()->create(['seo_title'=>$request->tag]);
                    return response()->json(array('id'=>$result->id,'name'=>$result->name));
                }
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Change status of Tag
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function status($id)
    {
        $tag = Tag::find($id);
        if($tag->status == 1)
        {
           $res = $tag->update(['status'=>0]);
        }
        else
        {
            $res = $tag->update(['status'=>1]);
        }
        if($res)
        {
            return redirect()->route('admin.tags')
                            ->with('success','Tag status change successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->posts()->detach();
        $tag->meta_element()->delete();
        $result = Tag::destroy($id);
        if($result )
        {
            return redirect()->route('admin.tags')
                            ->with('success','Tag deleted successfully.');
        }
    }
}
