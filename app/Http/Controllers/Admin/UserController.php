<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:list.user',['only'=>['index']]);
        $this->middleware('permission:create.user',['only'=>['create']]);
        $this->middleware('permission:store.user',['only'=>'store']);
        $this->middleware('permission:edit.user',['only'=>'edit']);
        $this->middleware('permission:update.user',['only'=>'update']);
        $this->middleware('permission:delete.user',['only'=>'destroy']);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('guard_name','web')->pluck('name','id')->toArray();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = User::create([
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => Hash::make($request->new_password)
        ]);
        if($user)
        {
            $user->roles()->sync($request->roles);
            return redirect()->route('admin.users')
                    ->with('success', ' User Created Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::where('guard_name','web')->pluck('name','id')->toArray();
        $user = User::find($id);

        if($user){
            return view('admin.users.edit',compact('user','roles'));
        }
        else
        {
            return redirect()->route('admin.users')
                        ->with('error',$id .' id dose not exist!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);
        $inputs['name']=$request->name;
        $inputs['email']=$request->email;
        if($request->new_password !='')
        {
            $inputs['password'] = Hash::make($request->new_password);
        }

        $result = $user->update($inputs);
        $user->roles()->sync($request->roles);
            if($result){

                return redirect()->route('admin.users.edit',$id)
                        ->with('success',' User updated successfully.');
            }
            else
            {
                return redirect()->route('admin.users.edit',$id)
                        ->with('error','Somethings is wrong!');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
