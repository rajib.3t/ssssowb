<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;
use Illuminate\Support\Facades\Auth;
use App\Post;
use Slug;
use Seo;
use DB;
use Setting;
class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:list.page',['only'=>['index']]);
        $this->middleware('permission:create.page',['only'=>['create']]);
        $this->middleware('permission:store.page',['only'=>['store']]);
        $this->middleware('permission:edit.page',['only'=>['edit']]);
        $this->middleware('permission:update.page',['only'=>'update']);
        $this->middleware('permission:delete.page',['only'=>['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Post::sortable()->where('post_type', 'page')->orderBy('created_at', 'desc')->paginate(Setting::get('row_in_admin_list'));
        //$pages = Post::where('post_type', 'page')->orderBy('created_at', 'desc')->get();
        return view('admin.pages.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $inputs['title'] = $request->title;
        $inputs['postable_id']=Auth::guard('admin')->user()->id;
        $inputs['postable_type'] = get_class(Auth::guard('admin')->user());
        $inputs['post_type']='page';
        $inputs['slug']=Slug::createPageSlug($request->title);
        $inputs['template']='page_templates.default';
        $inputs['status']=1;
        $inputs['parent']=0;
        $result = Post::create($inputs);
        $result->meta_element()->create(['seo_title'=>$request->title]);
        if($result){
            return redirect()->route('admin.pages.edit',$result->id)
                    ->with('success','Page created successfully.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Post::find($id);
        if($page)
        {
            return view('admin.pages.edit',compact('page'));
        }
        else
        {
            return redirect()->route('admin.pages')
                    ->with('error',$id.' page id is not exist');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {



        $page = Post::find($id);
        $inputs['title']= $request->title;
        $parent = $request->parent;
        if($page->parent != $parent)
        {
            $inputs['slug']=Slug::createPageSlug($request->title,$id,$parent);
            $inputs['parent']=$parent;
        }
        $inputs['content'] = $request->content;
        if($request->image  != '' && $request->image_id != ''){
            $inputs['image_id']=$request->image_id;
            //$banner_image = Post::find($id);
            $page->media()->sync([$request->image_id=>['alt_text'=>$request->image_alt]],false);
            //dd($a);
        }else if($request->image_id !=''){
            $inputs['image_id']='';
            //$banner_image = Post::find($id);
            $page->media()->detach($page->image_id);
        }
        if($request->modile_banner_image  != '' && $request->modile_banner_image_id != ''){
            $inputs['mobile_banner_id']=$request->modile_banner_image_id;
            //$modile_banner_image = Post::find($id);
            $page->media()->sync([$request->modile_banner_image_id=>['alt_text'=>$request->modile_banner_image_alt]],false);
        }else if($request->modile_banner_image_id !=''){
            $inputs['mobile_banner_id']='';
            //$modile_banner_image = Post::find($id);
            $page->media()->detach($page->mobile_banner_id);
        }
        $inputs['template']=$request->template;
        $inputs['status']=$request->status;
        $result = $page->update($inputs);

        Seo::updateElement($page ,$request);

        if($result){
            return redirect()->route('admin.pages.edit',$id)
                    ->with('success','Page edited successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Post::find($id);
        $page->media()->detach($page->mobile_banner_id);
        $page->media()->detach($page->image_id);
        $page->meta_element()->delete();
        $result = Post::destroy($id);
        if($result){

                return redirect()->route('admin.pages')
                        ->with('success','Page deleted successfully.');

        }
    }
}
