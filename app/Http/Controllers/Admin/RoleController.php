<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RolePermissionRequest;
use App\Role;
class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        
        $this->middleware('permission:list.role', ['only' => ['index']]);
        $this->middleware('permission:create.role',['only'=>['store']]);
        $this->middleware('permission:edit.role', ['only' => ['edit']]);
        $this->middleware('permission:update.role', ['only' => ['update']]);
        $this->middleware('permission:addpermission.torole',['only'=>['addRoleToPermissions']]);
        $this->middleware('permission:delete.role',['only'=>['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
      
        return view('admin.roles.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolePermissionRequest $request)
    {
        
        $role = Role::create([
            'name'=>$request->name,
            'guard_name'=>$request->guard
        ]);
        if($role)
        {
            return redirect()->route('admin.roles')
                    ->with('success','Role added successfully.');
        }
        else
        {
            return redirect()->route('admin.roles')
                    ->with('error','Something are wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        if($role)
        {
            return view('admin.roles.edit',compact('role'));
        }
        else
        {
            return redirect()->route('admin.roles')
                    ->with('error',$id .' id dose not exist!');   
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolePermissionRequest $request, $id)
    {
        $role = Role::find($id);
        $result = $role->update(
            [
                'name'=>$request->name,
                'guard_name'=>$request->guard
            ]
            );

        if($result)
        {
            return redirect()->route('admin.role.edit',$id)
                    ->with('success','Role updated successfully.');   
        }
    }

    /**
     * Add Permistion to the role
     */
    public function addRoleToPermissions(Request $request,$id)
    {
        $role = Role::find($id);
        if($request->permissions)
        {
            $result = $role->permissions()->sync($request->permissions);
            if($result)
            {
                return redirect()->route('admin.role.edit',$id)
                        ->with('success','Permission added to Role successfully.');   
            }
            else
            {
                return redirect()->route('admin.role.edit',$id)
                        ->with('error','Something are wrong!'); 
            }
        }
        else
        {
            $result = $role->permissions()->detach();
            if($result)
            {
                return redirect()->route('admin.role.edit',$id)
                        ->with('success','All Permission remove from Role successfully.');   
            }
            else
            {
                return redirect()->route('admin.role.edit',$id)
                        ->with('error','Something are wrong!'); 
            }
        }

        
       


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if($role)
        {
            $role->permissions()->detach();
            if($role->admins()->get()){
                $role->admins()->detach();
            }
            if($role->users()->get()){
                $role->users()->detach();
            }
            
            
            $result = Role::destroy($id);
            if($result)
            {
                return redirect()->route('admin.roles')
                        ->with('success','Role deleted successfully.');
            }
            else
            {
                return redirect()->route('admin.roles')
                    ->with('error','Somethings are wrong!');
            }
            
        }
        else
        {
            return redirect()->route('admin.roles')
                    ->with('error',$id .' id dose not exist!'); 
        }
    }
}
