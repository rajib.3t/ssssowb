<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Setting;
class PageController extends Controller
{

    public function index()
    {
        $undercons = Setting::get('under_construction');
        if($undercons == 'on')
        {
            return view('underconstruction.index');
        }
    }
}
