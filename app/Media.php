<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ['name','filename','url', 'alt','minetype','size'];

    public function option()
    {
        return $this->morphedByMany('App\Option', 'mediaable')->withPivot('alt_text');
    }

    public function admin()
    {
        return $this->morphedByMany('App\Admin', 'mediaable')->withPivot('alt_text');
    }

    public function posts()
    {
        return $this->morphedByMany('App\Post', 'mediaable')->withPivot('alt_text');
    }

    public function categories()
    {
        return $this->morphedByMany('App\Category', 'mediaable')->withPivot('alt_text');
    }
    
}
