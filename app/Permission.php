<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Role;
class Permission extends Model
{
    protected $fillable = ['guard_name','name','description'];

    /**
     * Get all roles for this permission
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'role_has_permissions')->withTimestamps();
    }

    /**
     * Gourd Wise List 
     */

     public function scopeFilter($query, $guard)
     {
        return $query->where('guard_name', $guard);
     }
}
