<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Schema;
use DB;
class MetaElement extends Model
{
    protected $fillable =['model_id','model_type','seo_title'];

    // Get all model releted model
    public function metaelementable()
    {
        return $this->morphTo();
    }

    /**
     * Get all field name
     */
    public function getTableColumns() 
    {
       
        $colums =  Schema::getColumnListing($this->getTable());
        //$colums =  $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
        
        $colums = array_diff($colums, ["id", "metaelementable_id",'metaelementable_type','created_at','updated_at']);
        return $colums;

    }
}
