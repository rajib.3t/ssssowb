<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomTraits\MediaTrait;
class Option extends Model
{
    use MediaTrait;
    protected $fillable = ['option_name', 'option_value'];


    
}
