<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomTraits\SeoMetaElements;
use App\CustomTraits\MediaTrait;
class Tag extends Model
{
    use SeoMetaElements,MediaTrait;
    protected $fillable = ['name','slug','status','content','image_id','mobile_image_id'];

    /**
     * Get The owning post
     */

    public function posts()
    {
        return $this->morphedByMany('App\Post', 'post_tag_relationships');
    }

    
}
