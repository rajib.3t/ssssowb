<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['name','status','menu_data'];

    /**
    * Get The owning Items
    */
    public function menu_items()
    {
        return $this->hasMany('App\MenuItem');
    }

    
}

