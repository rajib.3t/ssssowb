<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permission;

class Role extends Model
{
    protected $fillable = ['guard_name','name'];
    /**
     * Get all permission for role
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'role_has_permissions')->withTimestamps();
    }

    /**
     * Gourd Wise List 
     */

     public function scopeFilter($query, $guard)
     {
        return $query->where('guard_name', $guard);
     }

     /**
     * Get all of the users that are assigned this role.
     */
    public function users()
    {
        return $this->morphedByMany('App\User','model','model_has_roles','role_id','model_id');

    }


    /**
     * Get all of the admins that are assigned this role.
     */
    public function admins()
    {
        return $this->morphedByMany('App\Admin','model','model_has_roles','role_id','model_id');
    }
}
