<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\CustomTraits\HasPermissionsTrait;
use App\CustomTraits\MediaTrait;
use App\Post;
class Admin extends Authenticatable
{
    use Notifiable,HasPermissionsTrait,MediaTrait;
     

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get all of the user's post.
     */
    public function posts()
    {
        return $this->morphMany('App\Post','postable');
    }

    public function meta_details()
    {
        return $this->morphMany('App\UserMeta','model', 'model_type', 'model_id', 'id');
    }

    
}
