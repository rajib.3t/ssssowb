<?php

use Illuminate\Database\Seeder;
use App\Option;

class CreateOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Option::create(['option_name'=>'site_name','option_value'=>'My Site']);
        Option::create(['option_name'=>'tag_line','option_value'=>'My Site']);
        Option::create(['option_name'=>'site_lang','option_value'=>'en_IN']);
        Option::create(['option_name'=>'timezone','option_value'=>'Asia/Kolkata']);
        Option::create(['option_name'=>'under_construction','option_value'=>'on']);

    }
}
