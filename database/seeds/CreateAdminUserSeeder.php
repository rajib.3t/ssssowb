<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Hash;
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Admin::create([
        	'name' => 'Rajib Mondal',
        	'email' => 'rajib.3t@gmail.com',
        	'password' => Hash::make('12345678')
        ]);

        $role = Role::create([
            'name'=>'Super Admin',
            'guard_name'=>'admin'
        ]);
        $permissions = Permission::filter('admin')->pluck('id','id')->all();
        $role->permissions()->sync($permissions);
        $user->roles()->sync($role);

    }
}
