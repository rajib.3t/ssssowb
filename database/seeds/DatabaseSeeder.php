<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateOptionSeeder::class);
        $this->call(CreatePermissionSeeder::class);
        $this->call(CreateAdminUserSeeder::class);
        $this->call(CreateUserSeeder::class);
        $this->call(CreateCategorySeeder::class);
    }
}
