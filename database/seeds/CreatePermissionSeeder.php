<?php

use Illuminate\Database\Seeder;
use App\Permission;
class CreatePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'list.permission',
            'create.permission',
            'edit.permission',
            'update.permission',
            'addrole.topermission',
            'delete.permission',
            'list.role',
            'create.role',
            'edit.role',
            'update.role',
            'addpermission.torole',
            'delete.role',
            'list.post',
            'create.post',
            'store.post',
            'edit.post',
            'update.post',
            'delete.post',
            'list.page',
            'create.page',
            'store.page',
            'edit.page',
            'update.page',
            'delete.page',
            'list.category',
            'store.category',
            'edit.category',
            'update.category',
            'delete.category',
            'list.tag',
            'store.tag',
            'store.tag.ajax',
            'change.tag.status',
            'delete.tag',
            'list.media',
            'create.media',
            'store.media',
            'store.media.editor',
            'browse.media.editor',
            'delete.media.all',
            'delete.media',
            'list.menu',
            'change.menu.status',
            'store.menu',
            'edit.menu',
            'update.menu',
            'add.menu.item',
            'store.menu.item',
            'delete.menu',
            'list.adminuser',
            'create.adminuser',
            'store.adminuser',
            'edit.adminuser',
            'update.adminuser',
            'delete.adminuser',
            'adminuser.special.permission',
            'adminuser.special.permission.save',
            'list.user',
            'create.user',
            'store.user',
            'edit.user',
            'update.user',
            'delete.user',
            'setting.general',
            'setting.underConstruction',
            'setting.save'

         ];

         foreach ($permissions as $permission)
         {
            Permission::create(['name' => $permission,'guard_name'=>'admin']);
         }

       $permissionsweb = [
           'list.post'
       ];
       foreach ($permissionsweb as $permission)
       {
          Permission::create(['name' => $permission,'guard_name'=>'web']);
       }
    }
}
