<?php

use Illuminate\Database\Seeder;
use App\Category;
class CreateCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = Category::create([
        'name'=>'Uncategorized',
        'parent'=>0,
        'slug'=>'uncategorized',
        'status'=>1
        ]);
        $cat->meta_element()->create(['seo_title'=>'Uncategorized']);
    }
}
