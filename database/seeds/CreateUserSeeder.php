<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Hash;
class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'name' => 'Rajib Mondal',
        	'email' => 'rajib.keyin@gmail.com',
        	'password' => Hash::make('12345678')
        ]);

        $role_web = Role::create([
            'name'=>'Basic',
            'guard_name'=>'web'
        ]);
        $permissions = Permission::filter('web')->pluck('id','id')->all();
        $role_web->permissions()->sync($permissions);
        $user->roles()->sync($role_web);
    }
}
