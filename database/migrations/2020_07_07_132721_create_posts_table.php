<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('postable_id')->nullable();
            $table->string('postable_type')->nullable();
            $table->string('post_type');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('slug');
            $table->string('image_id')->nullable();
            $table->string('mobile_banner_id')->nullable();
            $table->string('template');
            $table->integer('parent')->nullable();
            $table->integer('menu_order')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
