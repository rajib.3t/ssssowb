<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('menu_id');
            $table->unsignedInteger('model_id')->nullable();
            $table->string('model_type')->nullable();
            $table->string('menu_type')->nullable();
            $table->string('label');
            $table->string('css_class')->nullable();
            $table->integer('menu_order')->nullable();
            $table->text('custom_url')->nullable();
            $table->integer('parent');
            $table->string('structure_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
