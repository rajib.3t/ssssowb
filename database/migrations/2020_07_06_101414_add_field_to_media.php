<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('filename')->after('name');
            $table->string('minetype')->nullable()->after('alt');
            $table->string('size')->nullable()->after('minetype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn('minetype');
            $table->dropColumn('size');
            $table->dropColumn('name');
            $table->dropColumn('filename');
        });
    }
}
