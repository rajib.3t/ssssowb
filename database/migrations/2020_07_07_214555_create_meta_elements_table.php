<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_elements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('metaelementable_id');
            $table->string('metaelementable_type');
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->mediumText('keywords')->nullable();
            $table->mediumText('robots')->nullable();
            $table->mediumText('revisit_after')->nullable();
            $table->mediumText('og_locale')->nullable();
            $table->mediumText('og_type')->nullable();
            $table->mediumText('og_image')->nullable();
            $table->mediumText('og_title')->nullable();
            $table->mediumText('og_url')->nullable();
            $table->mediumText('og_description')->nullable();
            $table->mediumText('og_site_name')->nullable();
            $table->mediumText('author')->nullable();
            $table->mediumText('canonical')->nullable();
            $table->mediumText('geo_region')->nullable();
            $table->mediumText('geo_placename')->nullable();
            $table->mediumText('geo_position')->nullable();
            $table->mediumText('ICBM')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_elements');
    }
}
