@php
   $logo_ob = Setting::get('site_logo_id',true);
   
   
@endphp
    @if($logo_ob)
        @php
            $logo = $logo_ob->media()->first();
        @endphp
    
    @endif
   @if(isset($logo))
        @php
            $url = $logo->url;
            $alt = $logo->pivot->alt_text
        @endphp
       
   @else
        @php
         $url = asset('admin-design/assets/images/logo.png');
       $alt  = 'My Site';   
        @endphp
       
    @endif   
   

<header class="header">
    <div class="logo-container">
        <a href="{{ url('admin') }}" class="logo">
            <img src="{{ $url}}" width="45" height="45" alt="{{ $alt }}">
        </a>
        <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <!-- start: search & user box -->
    <div class="header-right">

 

        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown" aria-expanded="false">
                @php
                        $profile_img = Auth::guard('admin')->user()->media()->find(Userdata::get(Auth::guard('admin')->user(),'profile_image'));
                        if(isset($profile_img)):
                            
                            $url = $profile_img->url;
                            $alt = $profile_img->pivot->alt_text;
                        else :
                            $url = asset('admin-design/assets/images/!logged-user.jpg');
                            $alt = 'Default';
                        endif
                    @endphp
                <figure class="profile-picture">
                    <img src="{{ $url}}" alt="{{ $alt }}" class="img-circle" data-lock-picture="{{ $url }}">
                </figure>
                <div class="profile-info" data-lock-name="{{ Auth::guard('admin')->user()->name }}" data-lock-email="{{ Auth::guard('admin')->user()->email }}">
                    <span class="name"> {{ Auth::guard('admin')->user()->name }}</span>
                    <span class="role">{{ Auth::guard('admin')->user()->roles()->first()->name }}</span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ route('admin.profile') }}"><i class="fa fa-user"></i> {{ __('My Profile') }}</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ route('admin.logout') }}"  onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> {{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>