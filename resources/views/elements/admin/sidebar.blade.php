<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            {{ __('Navigation') }}
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano has-scrollbar">
        <div class="nano-content" tabindex="0" style="right: -15px;">
            <nav id="menu" class="nav-main" role="navigation">
                @php
                    $route = Route::currentRouteName()
                @endphp
                <ul class="nav nav-main">
                    <li class="{!! active_tab_child('admin.dashboard',$route) !!}">
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>{{ __('Dashboard') }}</span>
                        </a>
                    </li>
                    <li class="nav-parent {!! active_tab($route,array('admin.posts','admin.posts.create','admin.posts.edit','admin.categories','admin.categories.edit','admin.tags')) !!}">
                        <a href="#">
                            <i class="fa fa-rss" aria-hidden="true"></i>
                            <span>{{ __('Posts') }}</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{!! active_tab_child('admin.posts',$route) !!}"><a href="{{ route('admin.posts') }}">{{ __('List') }}</a></li>
                            <li class="{!! active_tab_child('admin.posts.create',$route) !!}"><a href="{{ route('admin.posts.create') }}">{{ __('Add') }}</a></li>
                            <li class="{!! active_tab_child('admin.categories',$route) !!}"><a href="{{ route('admin.categories') }}">{{ __('Categories') }}</a></li>
                            <li class="{!! active_tab_child('admin.tags',$route) !!}"><a href="{{ route('admin.tags') }}">{{ __('Tags') }}</a></li>
                        </ul>
                    </li>
                    {{-- @permitgroup('admin,list.media,test')
                    <h1>test</h1>
                    @endpermitgroup --}}
                    <li class="nav-parent {!! active_tab($route,array('admin.media','admin.media.create')) !!}">
                        <a href="#">
                            <i class="fa fa-picture-o" aria-hidden="true"></i>
                            <span>{{ __('Media') }}</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{!! active_tab_child('admin.media',$route) !!}"><a href="{{ route('admin.media') }}">{{ __('List') }}</a></li>
                            <li class="{!! active_tab_child('admin.media.create',$route) !!}"><a href="{{ route('admin.media.create') }}">{{ __('Add') }}</a></li>
                        </ul>
                    </li>
                    <li class="nav-parent {!! active_tab($route,array('admin.pages','admin.pages.create','admin.pages.edit')) !!}">
                        <a href="#">
                            <i class="fa fa-copy" aria-hidden="true"></i>
                            <span>{{ __('Pages') }}</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{!! active_tab_child('admin.pages',$route) !!}"><a href="{{ route('admin.pages') }}">{{ __('List') }}</a></li>
                            <li class="{!! active_tab_child('admin.pages.create',$route) !!}"><a href="{{ route('admin.pages.create') }}">{{ __('Add') }}</a></li>
                        </ul>
                    </li>
                    <li class="{!! active_tab($route,array('admin.menus','admin.menus.edit','admin.menus.additems')) !!}">
                        <a href="{{ route('admin.menus') }}">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <span>{{ __('Menus') }}</span>
                        </a>
                    </li>
                    <li class="nav-parent {!! active_tab($route,array('admin.setting.general','admin.setting.underconstruction','admin.setting.miscellaneous')) !!}">
                        <a href="#">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                            <span>{{ __('Setting') }}</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{!! active_tab_child('admin.setting.general',$route) !!}"><a href="{{ route('admin.setting.general') }}">{{ __('General') }}</a></li>
                            <li class="{!! active_tab_child('admin.setting.underconstruction',$route) !!}"><a href="{{ route('admin.setting.underconstruction') }}">{{ __('Under Construction') }}</a></li>
                            <li class="{!! active_tab_child('admin.setting.miscellaneous',$route) !!}"><a href="{{ route('admin.setting.miscellaneous') }}">{{ __('Miscellaneous') }}</a></li>
                        </ul>
                    </li>
                        {{-- @permitgroup('admin','list.adminuser','list.adminuser')
                        <li>Test</li>
                        @endpermitgroup --}}
                    <li class="nav-parent {!! active_tab($route,array('admin.adminusers','admin.adminuser.create','admin.adminuser.edit')) !!}">
                        <a href="#">
                            <i class="fa fa-user-secret" aria-hidden="true"></i>
                            <span>{{ __('Admin Users') }}</span>
                        </a>
                        <ul class="nav nav-children">
                            @permit('admin','list.adminuser')
                            <li class="{!! active_tab_child('admin.adminusers',$route) !!}"><a href="{{ route('admin.adminusers') }}">{{ __('List') }}</a></li>
                            @endpermit
                            @permit('admin','create.adminuser')
                            <li class="{!! active_tab_child('admin.adminuser.create',$route) !!}"><a href="{{ route('admin.adminuser.create') }}">{{ __('Add') }}</a></li>
                            @endpermit
                        </ul>
                    </li>
                    <li class="nav-parent {!! active_tab($route,array('admin.users','admin.users.create','admin.users.edit')) !!}">
                        <a href="#">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <span>{{ __('Users') }}</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{!! active_tab_child('admin.users',$route) !!}"><a href="{{ route('admin.users') }}">{{ __('List') }}</a></li>
                            <li class="{!! active_tab_child('admin.users.create',$route) !!}"><a href="{{ route('admin.users.create') }}">{{ __('Add') }}</a></li>
                        </ul>
                    </li>
                    <li class="nav-parent {!! active_tab($route,array('admin.roles','admin.permissions','admin.role.edit','admin.permission.edit')) !!}">
                        <a href="#">
                            <i class="fa fa-shield" aria-hidden="true"></i>
                            <span>{{ __('ACL') }}</span>
                        </a>
                        <ul class="nav nav-children">



                            @permit('admin','list.role')
                            <li class="{!! active_tab_child('admin.roles',$route) !!}"><a href="{{ route('admin.roles') }}">{{ __('Roles') }}</a></li>
                            @endpermit
                            @permit('admin','list.permission')
                            <li class="{!! active_tab_child('admin.permissions',$route) !!}"><a href="{{ route('admin.permissions') }}">{{ __('Permissions') }}</a></li>
                            @endpermit
                        </ul>
                    </li>

                </ul>
            </nav>




        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>


    <div class="nano-pane" style="opacity: 1; visibility: visible;"><div class="nano-slider" style="height: 33px; transform: translate(0px, 194px);">
    </div>
</div>
</div>

</aside>
