@extends('layouts.master')
@section('title') Admin Users @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Admin Users') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>{{ __('Admin Users') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            
        </div>

        <h2 class="panel-title">{{ __('Admin User') }}</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th>{{ 'Sl' }}</th>
                    <th>{{ 'Name' }}</th>
                    <th>{{ 'Email' }}</th>
                    
                    
                    <th>{{ 'Action' }}</th>
                </tr>
            </thead>
            <tbody>
                @php ($i = 1)  
                @foreach ($users as $key =>$user)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    
                    <td>
                        <a href="{{ route('admin.adminuser.edit',$user->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        
                        <a href="{{ route('admin.categories.delete',$user->id) }}" onclick="return confirm('Are you sure you want delete ?');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>  

                       
                        
                    </td>
                </tr>   
                @endforeach
                
                
            </tbody>
        </table>
    </div>
</section>
@endsection
@push('cssvender')
    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
@endpush

@push('jsvender')
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
	<script src="{{ asset('admin-design/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
@endpush
@push('adminjs')
<script src="{{ asset('admin-design/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Admin User',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            new PNotify({
                    title: 'Admin User',
                    text: '{{ $message }}',
                    type: 'error'
                });
        </script>
    @endif
@endpush