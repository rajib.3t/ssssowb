@extends('layouts.master')
@section('title') Edit Page @endsection
@section('content')
<section class="panel">
    <div class="panel-body">
            <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                @if ($media)
                    @foreach ($media as $item)
                    <div class="isotope-item image col-sm-6 col-md-4 col-lg-3">
                        <div class="thumbnail">
                            <div class="thumb-preview">
                                <a class="thumb-image ckfileselect" onclick="returnFileUrl('{{$item->url }}')" data-url="{{ $item->url }}" data-alt="{{  $item->alt }}"href="{{ $item->url }}">
                                    <img src="{{ $item->url }}" class="img-responsive" alt="Project">
                                </a>
                                {{-- <div class="mg-thumb-options">
                                    <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                    <div class="mg-toolbar">
                                        <div class="mg-option checkbox-custom checkbox-inline">
                                            <input type="checkbox" id="file_8" value="1">
                                            <label for="file_8">SELECT</label>
                                        </div>
                                        <div class="mg-group pull-right">
                                         - <a href="#">EDIT</a> 
                                            <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                <i class="fa fa-caret-up"></i>
                                            </button>
                                            <ul class="dropdown-menu mg-menu" role="menu">
                                                <li><a href="#"><i class="fa fa-download"></i> Download</a></li>
                                                <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            {{-- <h5 class="mg-title text-weight-semibold">Hapiness<small>.png</small></h5>
                            <div class="mg-description">
                                <small class="pull-left text-muted">Websites</small>
                                <small class="pull-right text-muted">07/10/2016</small>
                            </div> --}}
                        </div>
                    </div>  
            
    


        @endforeach
    @endif
            </div>
</div>
</section>
@endsection
@push('adminjs')
<script>
    // Helper function to get parameters from the query string.
    function getUrlParam( paramName ) {
        var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
        var match = window.location.search.match( reParam );

        return ( match && match.length > 1 ) ? match[1] : null;
    }
    // Simulate user action of selecting a file to be returned to CKEditor.
    function returnFileUrl(url) {

        var funcNum = getUrlParam( 'CKEditorFuncNum' );
        var fileUrl = url;
        window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
        window.close();
    }
</script>
@endpush