@foreach ($media as $item)

    <div class="isotope-item image col-sm-6 col-md-4 col-lg-3">
        <div class="thumbnail">
            <div class="thumb-preview">
                <a class="thumb-image selectfile" data-push="{{ $name }}" data-url="{{ $item->url }}" data-id="{{ $item->id }}" data-alt="{{ $item->alt }}" href="javascript:void(0)" >
                    <img src="{{ $item->url }}" class="img-responsive" alt="{{ $item->alt }}">
                </a>

            </div>

        </div>
    </div>
@endforeach
