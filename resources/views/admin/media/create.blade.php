@extends('layouts.master')
@section('title') Add Media @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Add Media') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li><span>{{ __('Add Media') }}</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"></a>
    </div>
</header>
<div class="row">
    <div class="col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
                </div>

                <h2 class="panel-title">File Upload Drag'n Drop</h2>
            </header>
            <div class="panel-body">
                <form action="{{ route('admin.media.store') }}" class="dropzone dz-square dz-clickable" id="dropzone-example">
                    <div class="dz-default dz-message">
                        <span>Drop files here to upload</span>
                    </div>
                    @csrf
                </form>
            </div>
        </section>
    </div>
</div>
@endsection

@push('cssvender')
<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/dropzone/basic.css')}}" />
<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/dropzone/dropzone.css')}}" />
@endpush
@push('jsvender')
<script src="{{ asset('admin-design/assets/vendor/dropzone/dropzone.js')}}"></script>
@endpush
@push('adminjs')
    <script>
        $('#dropzone-example').on('success', function() {
            var args = Array.prototype.slice.call(arguments);

        // Look at the output in you browser console, if there is something interesting
        console.log(args);
        });
    </script>
@endpush
