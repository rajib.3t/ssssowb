@extends('layouts.master')
@section('title') Media Library @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Media Gallery') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li><span>{{ __('Media Gallery') }}</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"></a>
    </div>
</header>

<!-- start: page -->
<section class="content-with-menu content-with-menu-has-toolbar media-gallery">
    <div class="content-with-menu-container">
        <div class="inner-menu-toggle">
            <a href="#" class="inner-menu-expand" data-open="inner-menu">
                Show Bar <i class="fa fa-chevron-right"></i>
            </a>
        </div>

        <menu id="content-menu" class="inner-menu" role="menu">
            <div class="nano">
                <div class="nano-content">

                    <div class="inner-menu-toggle-inside">
                        <a href="#" class="inner-menu-collapse">
                            <i class="fa fa-chevron-up visible-xs-inline"></i><i class="fa fa-chevron-left hidden-xs-inline"></i> Hide Bar
                        </a>
                        <a href="#" class="inner-menu-expand" data-open="inner-menu">
                            Show Bar <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>

                    <div class="inner-menu-content">

                        <a class="btn btn-block btn-primary btn-md pt-sm pb-sm text-md">
                            <i class="fa fa-upload mr-xs"></i>
                            Upload Files
                        </a>

                        <hr class="separator" />

                        <div class="sidebar-widget m-none">
                            <div class="widget-header clearfix">
                                <h6 class="title pull-left mt-xs">Folders</h6>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-dark btn-sm btn-widget-act">Add Folder</a>
                                </div>
                            </div>
                            <div class="widget-content">
                                <ul class="mg-folders">
                                    <li>
                                        <a href="#" class="menu-item"><i class="fa fa-folder"></i> My Documents</a>
                                        <div class="item-options">
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" class="text-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-item"><i class="fa fa-folder"></i> Templates</a>
                                        <div class="item-options">
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" class="text-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-item"><i class="fa fa-folder"></i> Design</a>
                                        <div class="item-options">
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" class="text-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-item"><i class="fa fa-folder"></i> PSDs</a>
                                        <div class="item-options">
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" class="text-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-item"><i class="fa fa-folder"></i> Downloads</a>
                                        <div class="item-options">
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" class="text-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-item"><i class="fa fa-folder"></i> Photos</a>
                                        <div class="item-options">
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" class="text-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-item"><i class="fa fa-folder"></i> Projects</a>
                                        <div class="item-options">
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" class="text-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <hr class="separator" />

                        <div class="sidebar-widget m-none">
                            <div class="widget-header">
                                <h6 class="title">Labels</h6>
                                <span class="widget-toggle">+</span>
                            </div>
                            <div class="widget-content">
                                <ul class="mg-tags">
                                    <li><a href="#">Design</a></li>
                                    <li><a href="#">Projects</a></li>
                                    <li><a href="#">Photos</a></li>
                                    <li><a href="#">Websites</a></li>
                                    <li><a href="#">Documentation</a></li>
                                    <li><a href="#">Download</a></li>
                                    <li><a href="#">Images</a></li>
                                    <li><a href="#">Vacation</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </menu>
        <div class="inner-body mg-main">

            <div class="inner-toolbar clearfix">
                <ul>
                    <li>
                        <a href="#" id="mgSelectAll"><i class="fa fa-check-square"></i> <span data-all-text="Select All" data-none-text="Select None">Select All</span></a>
                    </li>
                    {{-- <li>
                        <a href="#"><i class="fa fa-pencil"></i> Edit</a>
                    </li> --}}
                    <li>
                        <a href="javascript:void(0)" onclick="deleteAll();"><i class="fa fa-trash-o"></i> Delete</a>
                    </li>
                    {{-- <li class="right" data-sort-source data-sort-id="media-gallery">
                        <ul class="nav nav-pills nav-pills-primary">
                            <li>
                                <label>Filter:</label>
                            </li>
                            <li class="active">
                                <a data-option-value="*" href="#all">All</a>
                            </li>
                            <li>
                                <a data-option-value=".document" href="#document">Documents</a>
                            </li>
                            <li>
                                <a data-option-value=".image" href="#image">Images</a>
                            </li>
                            <li>
                                <a data-option-value=".video" href="#video">Videos</a>
                            </li>
                        </ul>
                    </li> --}}
                </ul>
            </div>
            <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                @if ($media)
                    @foreach ($media as $item)
                    <div class="isotope-item image col-sm-6 col-md-4 col-lg-3">
                        <div class="thumbnail">
                            <div class="thumb-preview">
                                <a class="thumb-image" href="{{ $item->url }}">
                                    <img src="{{ $item->url }}" class="img-responsive" alt="Project">
                                </a>
                                <div class="mg-thumb-options">
                                    <div class="mg-zoom"><i class="fa fa-search"></i></div>
                                    <div class="mg-toolbar">
                                        <div class="mg-option checkbox-custom checkbox-inline">
                                            <input type="checkbox" id="file_{{ $item->id }}" name="selectall[]" value="{{ $item->id }}">
                                            <label for="file_{{ $item->id }}">SELECT</label>
                                        </div>
                                        <div class="mg-group pull-right">
                                            {{-- <a href="#">EDIT</a> --}}
                                            <button class="dropdown-toggle mg-toggle" type="button" data-toggle="dropdown">
                                                <i class="fa fa-caret-up"></i>
                                            </button>
                                            <ul class="dropdown-menu mg-menu" role="menu">
                                                {{-- <li><a href="#"><i class="fa fa-download"></i> Download</a></li> --}}
                                                <li><a href="{{ route('admin.media.delete',$item->id) }}" onclick="return confirm('Are you sure you want delete ?');"><i class="fa fa-trash-o"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <h5 class="mg-title text-weight-semibold">Hapiness<small>.png</small></h5>
                            <div class="mg-description">
                                <small class="pull-left text-muted">Websites</small>
                                <small class="pull-right text-muted">07/10/2016</small>
                            </div> --}}
                        </div>
                    </div>
                    @endforeach
                @endif

            </div>
            {{ $media->links() }}

        </div>
    </div>
</section>
<!-- end: page -->
@endsection
@push('jsvender')
<script src="{{ asset('admin-design/assets/vendor/isotope/isotope.js')}}"></script>
@endpush
@push('adminjs')
<script src="{{ asset('admin-design/assets/javascripts/pages/examples.mediagallery.js')}}"></script>
@endpush
@push('adminjs')
    <script>
        function deleteAll(){
            var token = "{{ csrf_token()  }}";
            var ids = [];
            $('input[name="selectall[]"]:checked').each( function () {
		        ids.push($(this).val())
	        });
            if(confirm('Are you sure you want delete ?')){
                $.ajax({
                    url:"{{ route('admin.media.delete.all') }}",
                    type:"POST",
                    dataType:"JSON",
                    data:{
                        ids:ids,
                        _token:token
                    },
                    success:function(res){
                        if(res.status == true){
                            new PNotify({
                                title: 'Media',
                                text: 'File deleted successfully!',
                                type: 'success'
                            });
                            setTimeout(function()
                                {
                                    location.reload();
                                }, 3000);

                        }
                    }
                })
            }

        }
    </script>
@endpush
