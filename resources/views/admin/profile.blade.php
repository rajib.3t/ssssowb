@extends('layouts.master')
@section('title') Profile @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Profile') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>{{ __('Profile') }}</span>
            </li>

        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
{!! Form::model($admin, ['method' => 'PATCH','route' => ['admin.profile']]) !!}
    <div class="col-sm-9">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>

                </div>

                <h2 class="panel-title">{{ __('Profile Information') }}</h2>
            </header>
            <div class="panel-body">
                <div class="form-group @error('name') has-error @enderror">
                    {{ Form::label('name', 'Name') }}

                    {{ Form::text('name',$admin->name,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
                    @error('name')
                        <label id="name" class="error" >{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group @error('email') has-error @enderror">
                    {{ Form::label('email', 'Email') }}

                    {{ Form::text('email',$admin->email,array('id'=>'email','placeholder'=>'','class'=>'form-control')) }}
                    @error('email')
                        <label id="emial-error" class="error" >{{ $message }}</label>
                    @enderror
                </div>

            </div>
        </section>
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>

                </div>

                <h2 class="panel-title">{{ __('Password Change') }}</h2>
            </header>
            <div class="panel-body">
                <div class="form-group @error('new_password') has-error @enderror">
                    {{ Form::label('new_password', 'New Password', array('class' => 'control-label')) }}
                    {{ Form::password('new_password',array('id'=>'password','placeholder'=>'','class'=>'form-control')) }}
                    @error('new_password')
                        <label id="new_password_error" class="error" >{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group @error('conf_password') has-error @enderror">
                    {{ Form::label('conf-password', 'Conf Password', array('class' => 'control-label')) }}
                    {{ Form::password('conf_password',array('id'=>'conf-password','placeholder'=>'','class'=>'form-control')) }}
                    @error('conf_password')
                        <label id="conf_password_error" class="error" >{{ $message }}</label>
                    @enderror
                </div>
            </div>
        </section>
    </div>
    <div class="col-sm-3">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>

                </div>

                <h2 class="panel-title">{{ __('Profile Image') }}</h2>
            </header>
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    @php
                        $profile_img = $admin->media()->find(Userdata::get($admin,'profile_image'));

                        if($profile_img):

                            $url = $profile_img->url;
                            $alt = $profile_img->pivot->alt_text;
                        else :
                            $url = asset('admin-design/assets/images/!logged-user.jpg');
                            $alt = 'Default';
                        endif
                    @endphp
                    <img src="{{ $url }}" class="rounded img-responsive" alt="{{ $alt }}">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">{{ $admin->name }}</span>
                        <span class="thumb-info-type">{{ $admin->roles()->first()->name }}</span>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('profile_image', 'Profile Image', array('class' => 'control-label')) }}
                    {!! Upload::go_upload('profile_image',$admin, Userdata::get($admin,'profile_image'),true) !!}
                </div>
                <div class="form-group">
                    <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Update Profile') }}">
                </div>
            </div>
        </section>
    </div>
{{ Form::close() }}
@endsection
{{-- Page Specific CSS Vendor --}}
@push('cssvender')
<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/select2/css/select2.css') }}" />
@endpush
{{-- Page Specific JS Vendor --}}
@push('jsvender')
    <script src="{{ asset('admin-design/assets/vendor/select2/js/select2.js') }}"></script>

@endpush
@push('adminjs')
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Profile',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
    <script>
        $("#roles").select2();
    </script>
@endpush
