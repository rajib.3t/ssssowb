@extends('layouts.master')
@section('title') {{ __('Admin Login')}}@endsection
@section('content_login')
@php
   $logo_ob = Setting::get('site_logo_id',true);
   
@endphp
        @if($logo_ob)
        @php
            $logo = $logo_ob->media()->first();
        @endphp

        @endif
   @if(isset($logo))
        @php
            $url = $logo->url;
            $alt = $logo->pivot->alt_text
        @endphp
       
   @else
        @php
         $url = asset('admin-design/assets/images/logo.png');
        $alt  = 'My Site';   
        @endphp
       
    @endif   
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="" class="logo pull-left">
            <img src="{{ $url }}" height="54" alt="{{ $alt }}" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
            </div>
            <div class="panel-body">
                <form action="{{ route('admin.login.submit') }}" method="POST">
                    @csrf
                    <div class="form-group mb-lg @error('email') has-error @enderror">
                        <label>{{ __('Email') }}</label>
                        <div class="input-group input-group-icon">
                            <input id="email" name="email" type="text" value="{{ old('email') }}" value="{{ old('email') }}"  class="form-control input-lg" />
                            @error('email')
                            <label id="email-error" class="error">{{ $message }}</label>
                            @enderror 
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-user"></i>
                                </span>
                            </span>
                            
                        </div>
                       
                                       
                    </div>

                    <div class="form-group mb-lg @error('password') has-error @enderror">
                        <label class="pull-left">Password</label>
                        {{-- <div class="clearfix">
                            <label class="pull-left">Password</label>
                            <a href="pages-recover-password.html" class="pull-right">Lost Password?</a>
                        </div> --}}
                        <div class="input-group input-group-icon">
                            <input  name="password" id="password"  type="password" class="form-control input-lg" />
                            @error('password')
                            <label id="password-error" class="error">{{ $message }}</label>
                            @enderror 
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                                <label for="remember">Remember Me</label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
                        </div>
                    </div>

                    {{-- <span class="mt-lg mb-lg line-thru text-center text-uppercase">
                        <span>or</span>
                    </span>

                    <div class="mb-xs text-center">
                        <a class="btn btn-facebook mb-md ml-xs mr-xs">Connect with <i class="fa fa-facebook"></i></a>
                        <a class="btn btn-twitter mb-md ml-xs mr-xs">Connect with <i class="fa fa-twitter"></i></a>
                    </div> --}}

                    {{-- <p class="text-center">Don't have an account yet? <a href="pages-signup.html">Sign Up!</a></p> --}}

                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright {!! date('Y') !!}. All Rights Reserved.</p>
    </div>
</section>
@endsection