@extends('layouts.master')
@section('title')
    {{ __('Under Construction Setting') }}
@endsection

@section('content')
<header class="page-header">
    <h2>{{ __('Under Construction Setting') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.setting') }}">
                    {{ __('Setting') }}
                </a>
            </li>
            <li>
                <span>{{ __('Under Construction') }}</span>>
            </li>
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            
        </div>

        <h2 class="panel-title">{{ __('Under Construction') }}</h2>
    </header>
    <div class="panel-body">
        {{  Form::open(array('route' => 'admin.setting.underconstruction.save','method' => 'POST','files'=> true))}}
        <div class="form-group">
            <label class="control-label col-sm-3">{{ __('Enable') }}</label>
            <div class="switch switch-lg switch-primary col-sm-9">
                    @php
                        $uncon = Setting::get('under_construction')
                    @endphp
                    @if($uncon=='on')
                        @php
                             $chk ='checked="checked"';
                        @endphp
                        
                    
                    @else 
                        @php
                           $chk =''; 
                        @endphp
                        
                    @endif
                
                <input type="checkbox" name="switch" data-plugin-ios-switch @php
                    echo $chk
                @endphp/>
                 
            </div>
        </div>
        <div class="col-md-12">
            <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Save') }}">
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@push('jsvender')
<script src="{{ asset('admin-design/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>

@endpush