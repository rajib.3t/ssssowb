@extends('layouts.master')
@section('title')
    {{ __('Miscellaneous Setting') }}
@endsection

@section('content')
<header class="page-header">
    <h2>{{ __('Miscellaneous Setting') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.setting') }}">
                    {{ __('Setting') }}
                </a>
            </li>
            <li>
                <span>{{ __('Miscellaneous') }}</span>>
            </li>
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
<div class="row">

    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>

                </div>

                <h2 class="panel-title">{{ __('Miscellaneous') }}</h2>
            </header>
            <div class="panel-body">

                    {{  Form::open(array('route' => 'admin.setting.miscellaneous.save','method' => 'POST','files'=> true))}}
                    <div class="col-md-6 form-group @error('row_in_admin_list') has-error @enderror">
                        {{ Form::label('row_in_admin_list', 'No of Row in Admin List') }}

                        {{ Form::number('row_in_admin_list',Setting::get('row_in_admin_list'),array('id'=>'row_in_admin_list','placeholder'=>'','class'=>'form-control')) }}
                        @error('row_in_admin_list')
                            <label id="row_in_admin_list_error" class="error" >{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="col-md-6 form-group @error('row_in_admin_list') has-error @enderror">
                        {{ Form::label('set_homepage', 'Set Front Page') }}

                        {{ Form::select('set_homepage',['1'=>'home','2'=>'page'],Setting::get('set_homepage'),array('id'=>'set_homepage','class'=>'form-control')) }}
                        @error('row_in_admin_list')
                            <label id="row_in_admin_list_error" class="error" >{{ $message }}</label>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Save') }}">
                    </div>
                    {{ Form::close() }}

            </div>

        </section>
    </div>
</div>
@endsection
@push('adminjs')
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Setting',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
@endpush
