@extends('layouts.master')
@section('title')
    {{ __('General Setting') }}
@endsection

@section('content')
<header class="page-header">
    <h2>{{ __('General Setting') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.setting') }}">
                    {{ __('Setting') }}
                </a>
            </li>
            <li>
                <span>{{ __('General') }}</span>>
            </li>
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
<div class="row">

    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
                </div>

                <h2 class="panel-title">{{ __('General') }}</h2>
            </header>
            <div class="panel-body">
                
                    {{  Form::open(array('route' => 'admin.setting.general.save','method' => 'POST','files'=> true))}}
                    <div class="col-md-6 form-group @error('site_name') has-error @enderror">
                        {{ Form::label('site_name', 'Website Name') }}
                        
                        {{ Form::text('site_name',Setting::get('site_name'),array('id'=>'site_name','placeholder'=>'Website Name','class'=>'form-control')) }}
                        @error('site_name')
                            <label id="site_name" class="error" >{{ $message }}</label>
                        @enderror 
                    </div>
                    <div class="col-md-6 form-group">
                        {{ Form::label('site_tag', 'Website Tag Line', array('class' => 'control-label')) }}
                        
                        {{ Form::text('site_tag',Setting::get('tag_line'),array('id'=>'site_tag','placeholder'=>'Website Tag line','class'=>'form-control')) }}
                    
                    </div>
                    <div class="col-md-6 form-group">
                        {{ Form::label('timezone', 'Timezone', array('class' => 'control-label')) }}
                        {{ Form::select('timezone',array_combine(timezone_identifiers_list(),timezone_identifiers_list()),Setting::get('timezone'),array('class'=>'form-control')) }}
                        
                    
                    </div>
                    <div class="col-md-6 form-group">
                        {{ Form::label('language', 'Language', array('class' => 'control-label')) }}
                        {!! Form::select('language',Setting::listLocat(),Setting::get('site_lang'),array('class'=>'form-control'))!!}
                        
                    
                    </div>
                    <div class="col-md-6 form-group">
                        {{ Form::label('logo', 'Logo', array('class' => 'control-label')) }} 
                        {!! Upload::go_upload('site_logo',Setting::get('site_logo_id',true)) !!} 
                    </div>
                    <div class="col-md-6 form-group">
                        {{ Form::label('favicon', 'Favicon', array('class' => 'control-label')) }} 
                        {!! Upload::go_upload('site_favicon',Setting::get('site_favicon_id',true)) !!} 
                    </div>
                    
                    <div class="col-md-12">
                        <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Save') }}">
                    </div>
                    {{ Form::close() }}
               
            </div>

        </section>
    </div>
</div>
@endsection
@push('adminjs')
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Setting',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
@endpush
