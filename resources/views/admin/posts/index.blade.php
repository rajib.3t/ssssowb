@extends('layouts.master')
@section('title') Posts @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Posts') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>{{ __('Posts') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
	<!-- start: page -->
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{ __('All Posts') }}</h2>
        </header>
        
            <div class="panel-body">
                
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            
                            <th>@sortablelink('title', 'Title')</th>
                            
                            <th>{{ __('Categories') }}</th>
                            <th>{{ __('Tags') }}</th>
                            <th>@sortablelink('status', 'Status')</th>
                            
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php ($i = 1)  
                        @foreach ($posts as $key =>$post)
                        <tr>
                            
                            <td>{{ $post->title }}</td>
                            <td>{!! implode(',',$post->category()->pluck('name')->toArray()) !!}</td>
                            <td>{!! implode(',',$post->tags()->pluck('name')->toArray()) !!}</td>
                            <td>@if($post->status == '1')
                                {{__('publish')}}
                            @else
                                {{__('draft')}}
                            @endif

                            </td>
                            <td>
                                <a href="{{ route('admin.posts.edit',$post->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{ route('admin.posts.delete',$post->id) }}" onclick="return confirm('Are you sure you want delete ?');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>   
                        @endforeach
                        
                        
                    </tbody>
                </table>
            </div>
            <p>
                Displaying {{$posts->count()}} of {{ $posts->total() }} post(s).
            </p>
            {!! $posts->onEachSide(5)->appends(Request::except('page'))->render() !!}
        </section>
    </section>
<!-- end: page -->
@endsection
@push('cssvender')
    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
@endpush

@push('jsvender')
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
	<script src="{{ asset('admin-design/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
@endpush

@push('adminjs')
<script src="{{ asset('admin-design/assets/javascripts/tables/examples.datatables.default.js')}}"></script>

@if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Post',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
@endif
@if ($message = Session::get('error'))
        <script>
            new PNotify({
                    title: 'Post',
                    text: '{{ $message }}',
                    type: 'error'
                });
        </script>
@endif
@endpush