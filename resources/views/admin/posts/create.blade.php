@extends('layouts.master')
@section('title') Create Post @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Create Post') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.posts') }}">
                <span>{{ __('Posts') }}</span>
                </a>
            </li>
            <li>
                <span>{{ __('Create') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
<div class="row">

    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                    
                </div>

                <h2 class="panel-title">{{ __('Create Post') }}</h2>
            </header>
            <div class="panel-body">
                {{  Form::open(array('route' => 'admin.posts.save','method' => 'POST','files'=> true))}}
                    <div class="form-group @error('title') has-error @enderror">
                        {{ Form::label('title', 'Post Title') }}
                        
                        {{ Form::text('title','',array('id'=>'page_title','placeholder'=>'Title...','class'=>'form-control')) }}
                        @error('title')
                            <label id="title-error" class="error" >{{ $message }}</label>
                        @enderror 
                    </div>
                    <div class=" form-group @error('categories') has-error @enderror">
                        {{ Form::label('categories', 'Categories', array('class' => 'control-label')) }}
                        {!! Form::select('categories[]',$categories,1,array('multiple'=>'multiple','class'=>'form-control'))!!}
                        @error('categories')
                            <label id="categories-error" class="error" >{{ $message }}</label>
                        @enderror 
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Save') }}">
                    </div>
                {{ Form::close() }}
            </div>
        </section>
    </div>
</div>
@endsection
