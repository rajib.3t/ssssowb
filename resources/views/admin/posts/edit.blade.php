@extends('layouts.master')
@section('title') Edit Post @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Edit Post') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.posts') }}">
                <span>{{ __('Posts') }}</span>
                </a>
            </li>
            <li>
                <span>{{ __('Edit') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
{!! Form::model($post, ['method' => 'POST','route' => ['admin.posts.update', $post->id]]) !!}


<div class="col-sm-9">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{ __('Post Edit') }}</h2>
        </header>
        
        <div class="panel-body">
                <div class="form-group @error('title') has-error @enderror">
                    {{ Form::label('title', 'Title') }}
                    
                    {{ Form::text('title',$post->title,array('id'=>'title','placeholder'=>'','class'=>'form-control')) }}
                    @error('title')
                        <label id="site_name" class="error" >{{ $message }}</label>
                    @enderror 
                </div>
                <div class="form-group">
                    <p><strong>Permalink : </strong><span><a href="{{ url('/').'/' }}{{ Slug::createPageUrl($post->id) }}">{{ url('/').'/' }}{{  Slug::createPageUrl($post->id,true) ? Slug::createPageUrl($post->id,true).'/' :'' }}<span id="editable-post-name">{{ $post->slug }}</span></a></span><button type="button" class="btn btn-default" aria-label="Edit permalink">Edit</button></p>
                </div>
                <div class="form-group">
                    {{ Form::label('content', 'Content') }}
                    {!! Form::textarea('content', $post->content, ['class' => 'post_content','id'=>'post_content']) !!}
                </div>
                
            
            
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{  __('Seo Meta Details')}}</h2>
        </header>
        <div class="panel-body">
            {!! Seo::getElements($post) !!}
        </div>
    </section>
</div>
<div class="col-sm-3">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
            <h2 class="panel-title">{{ __('Attributes') }}</h2>
        </header>
        <div class="panel-body">
            
            <div class=" form-group">
                {{ Form::label('parent', 'Parent', array('class' => 'control-label')) }}
                {!! Form::select('parent',Slug::getPageParent($post->id,'post'),$post->parent,array('class'=>'form-control'))!!}
            </div>
            <div class=" form-group">
                {{ Form::label('status', 'Status', array('class' => 'control-label')) }}
                {!! Form::select('status',array(1=>'Publish',0=>'Draft'),$post->status,array('class'=>'form-control'))!!}
            </div>
            <div class="form-group">
                <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Update') }}">
            </div>
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
            <h3  class="panel-title">{{ __('Categories') }}</h3>
        </header>
        <div class="panel-body">
            <div class=" form-group">
                {{ Form::label('categories', 'Categories', array('class' => 'control-label')) }}
                {!! Form::select('categories[]',$categories,$post->category()->pluck('id')->toArray(),array('multiple'=>'multiple','class'=>'form-control','id'=>'categories'))!!}
            </div>
            <div class=" form-group">
                {{ Form::label('new-category', 'New Category', array('class' => 'control-label')) }}
                {{ Form::text('new-category',null,array('id'=>'new-category','placeholder'=>'','class'=>'form-control')) }}
            </div>
            <div class=" form-group">
                {{ Form::label('cat-parent', 'Categories', array('class' => 'control-label')) }}
                {!! Form::select('cat-parent',$parent_categories,0,array('class'=>'form-control','id'=>'new-cat'))!!}
            </div>
            <div class="form-group">
                <input type="button" onclick = "add_new_category()" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add Category') }}">
            </div>

        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
            <h3  class="panel-title">{{ __('Tags') }}</h3>
        </header>
        <div class="panel-body">
            <div class=" form-group">
                {{ Form::label('tags', 'Tags', array('class' => 'control-label')) }}
                {!! Form::select('tags[]',$tags,$post->tags()->pluck('id')->toArray(),array('multiple'=>'multiple','class'=>'form-control','id'=>'tags'))!!}
            </div>
            <div class=" form-group">
                {{ Form::label('new-tag', 'New Tag', array('class' => 'control-label')) }}
                {{ Form::text('new-tag',null,array('id'=>'new-tag','placeholder'=>'','class'=>'form-control')) }}
            </div>
            <div class="form-group">
                <input type="button" onclick = "add_new_tag()" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add Tag') }}">
            </div>
        </div>
    </section>

    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
            <h2 class="panel-title">{{ __('Banner Images') }}</h2>
        </header>
        <div class="panel-body">
            <div class="form-group">
                {{ Form::label('image', 'Banner', array('class' => 'control-label')) }} 
                {!! Upload::go_upload('image',$post,'image_id') !!} 
            </div>
             <div class="form-group">
                {{ Form::label('modile_banner_image', 'Mobile Banner', array('class' => 'control-label')) }} 
                {!! Upload::go_upload('modile_banner_image',$post,'mobile_banner_id') !!} 
            </div> 
        </div>
    </section>
</div>
{{ Form::close() }}
@endsection
{{-- Page Specific CSS Vendor --}}
@push('cssvender')
<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/select2/css/select2.css') }}" />
@endpush
{{-- Page Specific JS Vendor --}}
@push('jsvender')
    <script src="{{ asset('admin-design/assets/vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin-design/ckeditor/ckeditor.js') }}"></script>
@endpush
@push('adminjs')
    {{-- Ckeditor --}}
    <script>
        $(document).ready(function(){
                if($('#post_content').length > 0){
            CKEDITOR.replace( 'post_content', {
                filebrowserUploadUrl: "{{route('admin.media.ckeditor', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form',
                filebrowserBrowseUrl:"{{route('admin.media.ckbrowse', ['_token' => csrf_token() ])}}"
            });
            CKEDITOR.on('dialogDefinition', function (ev) {
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                
                if (dialogName == 'image') {
                    var onOk = dialogDefinition.onOk;
                    dialogDefinition.onOk=function(e){
                        var input = this.getContentElement( 'info', 'txtUrl' ),
                        imageSrcUrl = input.getValue();
                        //! Manipulate imageSrcUrl and set it
                        input.setValue( imageSrcUrl );
                        onOk && onOk.apply( this, e );
                        
                    }
                }
                });
                }
            });
    </script>
    {{-- Notification  --}}
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Post',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
    {{--  New Category add with ajax--}}
    <script>
        function add_new_category(){
            var name = $('input[name="new-category"]').val();
            var parent = $('select[name="cat-parent"]').val();
            var token = "{{ csrf_token()  }}";
            if(name !=''){
                $.ajax({
                    url:"{{ route('admin.categories.save') }}",
                    type:"POST",
                    dataType:"JSON",
                    data:{
                        'name':name,
                        'parent':parent,
                        '_token':token
                    },
                    success:function(res){
                        if(res.success == false){
                            new PNotify({
                                title: 'Category',
                                text: res.errors.name,
                                type: 'error'
                            });
                        }            
                        $('#categories').append('<option value="'+res.id+'" selected="selected">'+res.name+'</option>') 
                        $('input[name="new-category"]').val(''); 
                        $('select[name="cat-parent"]').val(0);

                    }
                })
            }else{
                new PNotify({
                    title: 'Post',
                    text: 'Plase Enter Category Name',
                    type: 'error'
                });
            }
        }
    </script>
    {{-- Use Select2 for Tag --}}
    <script>
        $("#tags").select2();
            
    function add_new_tag(){
      var tag = $("#new-tag").val();
      var token = "{{ csrf_token()  }}";
      if(tag != ''){

        $.ajax({
            url:"{{ route('admin.tags.ajax') }}",
            type:"POST",
            dataType:"JSON",
            data:{
                tag:tag,
                _token:token
            },
            success:function(res){
                
                //Set the value, creating a new option if necessary
                if ($("#tags").find("option[value=" + res.id + "]").length) {
                    
                    $("#tags").val(res.id).trigger("change");
                } else { 
                    // Create the DOM option that is pre-selected by default
                    var newTag = new Option(res.name, res.id, true, true);
                    // Append it to the select
                    $("#tags").append(newTag).trigger('change');
                }
                $("#new-tag").val('');
            }
        })
      }
       
    }
    </script>
@endpush