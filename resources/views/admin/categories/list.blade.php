@extends('layouts.master')
@section('title') Categories @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Categories') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>{{ __('Categories') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
<div class="col-sm-12">
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            
        </div>

        <h2 class="panel-title">{{ __('Add Category') }}</h2>
    </header>
    <div class="panel-body">
        {{  Form::open(array('route' => 'admin.categories.save','method' => 'POST','files'=> true))}}
        <div class="form-group @error('name') has-error @enderror">
            {{ Form::label('name', 'Name') }}
            
            {{ Form::text('name',null,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
            @error('name')
                <label id="name" class="error" >{{ $message }}</label>
            @enderror 
        </div>
        <div class=" form-group">
            {{ Form::label('parent', 'Parent', array('class' => 'control-label')) }}
            {!! Form::select('parent',$parent,null,array('class'=>'form-control'))!!}
        </div>
        <div class="form-group">
            <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add') }}">
        </div>
        {{ Form::close() }}
    </div>
</section>
</div>
<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{ __('Categories') }}</h2>
        </header>
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Status</th>
                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php ($i = 1)  
                    @foreach ($categories as $key =>$cat)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $cat->name }}</td>
                        <td>{{ $cat->slug }}</td>
                        <td>@if($cat->status == '1')
                            {{__('publish')}}
                        @else
                            {{__('draft')}}
                        @endif

                        </td>
                        <td>
                            <a href="{{ route('admin.categories.edit',$cat->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            @if ($cat->id != 1)
                            
                            <a href="{{ route('admin.categories.delete',$cat->id) }}" onclick="return confirm('Are you sure you want delete ?');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>  

                            @endif
                            
                        </td>
                    </tr>   
                    @endforeach
                    
                    
                </tbody>
            </table>
        </div>
    </section>
</div>
@endsection
@push('cssvender')
    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
@endpush

@push('jsvender')
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
	<script src="{{ asset('admin-design/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
@endpush
@push('adminjs')
<script src="{{ asset('admin-design/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Category',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            new PNotify({
                    title: 'Category',
                    text: '{{ $message }}',
                    type: 'error'
                });
        </script>
    @endif
@endpush