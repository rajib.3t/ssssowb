@extends('layouts.master')
@section('title') Edit Category @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Edit Category') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.categories') }}">
                    <span>{{ __('Categories') }}</span>
                </a>
            </li>
            <li>
                <span>{{ __('Edit Category') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>

    {!! Form::model($category, ['method' => 'POST','route' => ['admin.categories.update', $category->id]]) !!}
    <div class="col-sm-9">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                    
                </div>
        
                <h2 class="panel-title">{{ __('Edit Category') }}</h2>
            </header>
            <div class="panel-body">
                <div class="form-group @error('name') has-error @enderror">
                    {{ Form::label('name', 'Name') }}
                    
                    {{ Form::text('name',$category->name,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
                    @error('name')
                        <label id="name-error" class="error" >{{ $message }}</label>
                    @enderror 
                </div>
                <div class="form-group">
                    {{ Form::label('content', 'Content') }}
                    {!! Form::textarea('content', $category->descriptions, ['class' => 'post_content','id'=>'content']) !!}
                </div>
            </div>
        </section>
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                    
                </div>
        
                <h2 class="panel-title">{{  __('Seo Meta Details')}}</h2>
            </header>
            <div class="panel-body">
                {!! Seo::getElements($category) !!}
            </div>
        </section>
    </div>
    <div class="col-sm-3">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                    
                </div>
        
                <h2 class="panel-title">{{ __('Attributes') }}</h2>
            </header>
            <div class="panel-body">
                <div class=" form-group">
                    {{ Form::label('parent', 'Parent', array('class' => 'control-label')) }}
                    {!! Form::select('parent',$parent,$category->parent,array('class'=>'form-control'))!!}
                </div>
                <div class=" form-group">
                    {{ Form::label('status', 'Stutus', array('class' => 'control-label')) }}
                    {!! Form::select('status',array(1=>'Publish',0=>'Draft'),$category->status,array('class'=>'form-control'))!!}
                </div>
                <div class="form-group">
                    <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Update') }}">
                </div>
            </div>
        </section>
    </div>
    {{ Form::close() }}

@endsection

@push('jsvender')
    <script src="{{ asset('admin-design/ckeditor/ckeditor.js') }}"></script>
@endpush
@push('adminjs')
    <script>
        $(document).ready(function(){
                if($('#content').length > 0){
            CKEDITOR.replace( 'content', {
                filebrowserUploadUrl: "{{route('admin.media.ckeditor', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form',
                filebrowserBrowseUrl:"{{route('admin.media.ckbrowse', ['_token' => csrf_token() ])}}"
            });
            CKEDITOR.on('dialogDefinition', function (ev) {
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                
                if (dialogName == 'image') {
                    var onOk = dialogDefinition.onOk;
                    dialogDefinition.onOk=function(e){
                        var input = this.getContentElement( 'info', 'txtUrl' ),
                        imageSrcUrl = input.getValue();
                        //! Manipulate imageSrcUrl and set it
                        input.setValue( imageSrcUrl );
                        onOk && onOk.apply( this, e );
                        
                    }
                }
                });
                }
            });
    </script>

    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Category',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
@endpush