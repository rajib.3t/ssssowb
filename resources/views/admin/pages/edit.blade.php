@extends('layouts.master')
@section('title') Edit Page @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Edit Page') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.pages') }}">
                <span>{{ __('Pages') }}</span>
                </a>
            </li>
            <li>
                <span>{{ __('Edit') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
{!! Form::model($page, ['method' => 'POST','route' => ['admin.pages.update', $page->id]]) !!}


<div class="col-sm-9">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{ __('Page Edit') }}</h2>
        </header>
        
        <div class="panel-body">
                <div class="form-group @error('title') has-error @enderror">
                    {{ Form::label('title', 'Title') }}
                    
                    {{ Form::text('title',$page->title,array('id'=>'title','placeholder'=>'','class'=>'form-control')) }}
                    @error('title')
                        <label id="site_name" class="error" >{{ $message }}</label>
                    @enderror 
                </div>
                <div class="form-group">
                    <p><strong>Permalink : </strong><span><a href="{{ url('/').'/' }}{{ Slug::createPageUrl($page->id) }}">{{ url('/').'/' }}{{  Slug::createPageUrl($page->id,true) ? Slug::createPageUrl($page->id,true).'/' :'' }}<span id="editable-post-name">{{ $page->slug }}</span></a></span><button type="button" class="btn btn-default" aria-label="Edit permalink">Edit</button></p>
                </div>
                <div class="form-group">
                    {{ Form::label('content', 'Content') }}
                    {!! Form::textarea('content', $page->content, ['class' => 'page_content','id'=>'page_content']) !!}
                </div>
                
            
            
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{  __('Seo Meta Details')}}</h2>
        </header>
        <div class="panel-body">
            {!! Seo::getElements($page) !!}
        </div>
    </section>
</div>
<div class="col-sm-3">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
            <h2 class="panel-title">{{ __('Page Attributes') }}</h2>
        </header>
        <div class="panel-body">
            <div class="form-group">
                {{ Form::label('template', 'Template', array('class' => 'control-label')) }}
                {!! Form::select('template',Setting::getTemplates('page'),$page->template,array('class'=>'form-control'))!!}
            </div>
            <div class=" form-group">
                {{ Form::label('parent', 'Parent', array('class' => 'control-label')) }}
                {!! Form::select('parent',Slug::getPageParent($page->id),$page->parent,array('class'=>'form-control'))!!}
            </div>
            <div class=" form-group">
                {{ Form::label('status', 'Stutus', array('class' => 'control-label')) }}
                {!! Form::select('status',array(1=>'Publish',0=>'Draft'),$page->status,array('class'=>'form-control'))!!}
            </div>
            <div class="form-group">
                <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Update') }}">
            </div>
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
            <h2 class="panel-title">{{ __('Banner Images') }}</h2>
        </header>
        <div class="panel-body">
            <div class="form-group">
                {{ Form::label('image', 'Banner', array('class' => 'control-label')) }} 
                {!! Upload::go_upload('image',$page,'image_id') !!} 
            </div>
             <div class="form-group">
                {{ Form::label('modile_banner_image', 'Mobile Banner', array('class' => 'control-label')) }} 
                {!! Upload::go_upload('modile_banner_image',$page,'mobile_banner_id') !!} 
            </div> 
        </div>
    </section>
</div>
{{ Form::close() }}
@endsection
@push('jsvender')
    <script src="{{ asset('admin-design/ckeditor/ckeditor.js') }}"></script>
@endpush
@push('adminjs')
    <script>
        $(document).ready(function(){
                if($('#page_content').length > 0){
            CKEDITOR.replace( 'page_content', {
                filebrowserUploadUrl: "{{route('admin.media.ckeditor', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form',
                filebrowserBrowseUrl:"{{route('admin.media.ckbrowse', ['_token' => csrf_token() ])}}"
            });
            CKEDITOR.on('dialogDefinition', function (ev) {
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                
                if (dialogName == 'image') {
                    var onOk = dialogDefinition.onOk;
                    dialogDefinition.onOk=function(e){
                        var input = this.getContentElement( 'info', 'txtUrl' ),
                        imageSrcUrl = input.getValue();
                        //! Manipulate imageSrcUrl and set it
                        input.setValue( imageSrcUrl );
                        onOk && onOk.apply( this, e );
                        
                    }
                }
                });
                }
            });
    </script>

    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Page',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
@endpush