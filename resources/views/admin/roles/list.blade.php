@extends('layouts.master')
@section('title') Roles @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Roles') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>{{ __('Role') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            
        </div>

        <h2 class="panel-title">{{ __('Add Role') }}</h2>
    </header>
    <div class="panel-body">
        {{  Form::open(array('route' => 'admin.role.save','method' => 'POST','files'=> true))}}
        <div class="form-group col-sm-6 @error('name') has-error @enderror">
            {{ Form::label('name', 'Name') }}
            
            {{ Form::text('name',null,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
            @error('name')
                <label id="name-error" class="error" >{{ $message }}</label>
            @enderror 
        </div>
        <div class=" form-group col-sm-6 @error('guard') has-error @enderror">
            @php($guard_arr = config('auth.guards'))
            @php($guards = array_keys($guard_arr))
            @php($guards = array_combine($guards, $guards))
            {{ Form::label('guard', 'Guard', array('class' => 'control-label')) }}
            {!! Form::select('guard',$guards,null,array('class'=>'form-control'))!!}
            @error('guard')
                <label id="guard-error" class="error" >{{ $message }}</label>
            @enderror 
        </div>
        <div class="form-group col-lg-4">
          
            <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add') }}">
        </div>
        {{ Form::close() }}
    </div>
</section>
@foreach ($guards as $guard)
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            
        </div>

        <h2 class="panel-title">{{ ucwords(str_replace("-"," ",$guard)) }} {{ __(' Guard' ) }}</h2>
    </header>
    <div class="panel-body">
        @php($roles = \App\Role::filter($guard)->get())
         
        

        <table class="table table-bordered table-striped mb-none" id="datatable-{{ $guard }}">
            <thead>
                <tr>
                    <th>{{ __('Sl') }}</th>
                    <th>{{ __('Name') }}</th>
                    
                    <th>{{ __('Guard') }}</th>
                    
                    <th>{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @php ($i = 1)  
                    @foreach ($roles as $key =>$role)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $role->name }}</td>
                        
                        <td>
                            {{ $role->guard_name }}

                        </td>
                        <td>
                            <a href="{{ route('admin.role.edit',$role->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            
                            
                            <a href="{{ route('admin.role.delete',$role->id) }}" onclick="return confirm('Are you sure you want delete ?');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>  

                            
                            
                        </td>
                    </tr>   
                    @endforeach
                
                
            </tbody>
        </table>
        
    </div>
</section>
@endforeach

@endsection
@push('cssvender')
    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
@endpush

@push('jsvender')
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
	<script src="{{ asset('admin-design/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script src="{{ asset('admin-design/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
@endpush
@push('adminjs')
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: ' Role',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            new PNotify({
                    title: ' Role',
                    text: '{{ $message }}',
                    type: 'error'
                });

        </script>
    @endif
    <script>
        $(document).ready(function(){
                $('.table').dataTable();
        });
        </script>
@endpush