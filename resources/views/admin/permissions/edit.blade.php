@extends('layouts.master')
@section('title') Edit Permission @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Edit Permission') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.permissions') }}">
                    <span>{{ __('Permission') }}</span>
                </a>
            </li>
            <li>
                <span>{{ __('Edit Permission') }}</span>
            </li>

        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>



        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>

                </div>

                <h2 class="panel-title">{{ __('Edit Permission') }}</h2>
            </header>
            <div class="panel-body">
                {!! Form::model($permission, ['method' => 'POST','route' => ['admin.permission.update', $permission->id]]) !!}
                <div class="form-group col-sm-6 @error('name') has-error @enderror">
                    {{ Form::label('name', 'Name') }}

                    {{ Form::text('name',null,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
                    @error('name')
                        <label id="name-error" class="error" >{{ $message }}</label>
                    @enderror
                </div>
                <div class=" form-group col-sm-6 @error('guard') has-error @enderror">
                    @php($guard_arr = config('auth.guards'))
                    @php($guards = array_keys($guard_arr))
                    @php($guards = array_combine($guards, $guards))
                    {{ Form::label('guard', 'Guard', array('class' => 'control-label')) }}
                    {!! Form::select('guard',$guards,$permission->guard_name,array('class'=>'form-control'))!!}
                    @error('guard')
                        <label id="guard-error" class="error" >{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group col-sm-12 @error('description') has-error @enderror">
                    {{ Form::label('descrtptions','Descriptions',array('class'=>'control-label')) }}
                    {{ Form::textarea('description',$permission->description,array('rows' => 4,'id'=>'descrtptions','placeholder'=>'','class'=>'form-control')) }}
                    @error('description')
                        <label id="description-error" class="error" >{{ $message }}</label>
                    @enderror
                </div>
                <div class="form-group col-lg-4">

                    <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Update') }}">
                </div>
                {{ Form::close() }}
            </div>

        </section>
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>

                </div>

                <h2 class="panel-title">{{ __('Add Roles to Permission') }}</h2>
            </header>
            <div class="panel-body">
                {{  Form::open(array('route' => array('admin.permissionToroles',$permission->id),'method' => 'POST','files'=> true))}}
                @php($roles = \App\Role::filter($permission->guard_name)->get()->filter())
                @foreach ($roles as $item)

                <div class="form-group col-sm-8">
                    <div class="checkbox-custom checkbox-default">
                    @php($per = $item->permissions()->where(['permission_id'=>$permission->id])->get())

                    @if ( count($per) > 0)
                        @php($val = true)
                    @else
                        @php($val = null)
                    @endif

                    {{ Form::checkbox('roles[]', $item->id,$val,array('id'=>'role-'.$item->id)) }}
                    {{ Form::label('role-'.$item->id, $item->name)}}
                    </div>
                </div>
                <div class="col-sm-4">
                    {{ $item->description }}
                </div>
                @endforeach
                <div class="form-group col-sm-12">

                    <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add Roles') }}">
                </div>
                {{ Form::close() }}
            </div>
        </section>




@endsection


@push('adminjs')


    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Permission',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            new PNotify({
                    title: ' Role',
                    text: '{{ $message }}',
                    type: 'error'
                });

        </script>
    @endif
@endpush
