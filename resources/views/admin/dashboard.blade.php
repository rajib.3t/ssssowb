@extends('layouts.master')
@section('title') Dashboard @endsection
@section('content')
    <header class="page-header">
        <h2>Dashboard</h2>

        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ url('admin') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                
            </ol>

            <a class="sidebar-right-toggle" ></a>
        </div>
    </header>
    <div class="row">
        <div class="col-md-6 col-lg-12 col-xl-6">
            <section class="panel">
                <div class="panel-body">
                </div>
            </section>
        </div>
    </div>
@endsection