@extends('layouts.master')
@section('title') Add Menu Items @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Add Menu Items') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.menus') }}">
                    <span>{{ __('Menus') }}</span>
                </a>
            </li>
            <li>
                <span>{{ __('Add Menu Items') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{ __('Add Items to ') }}{{ $menu->name }}{{ __(' Menu') }}</h2>
        </header>
        <div class="panel-body">
            <div class="col-sm-4">
                <div class="pages">
                    <div class=" form-group">
                        {{ Form::label('pages', 'Pages', array('class' => 'control-label')) }}
                        {!! Form::select('pages[]',$pages,null,array('multiple'=>'multiple','class'=>'form-control','id'=>'pages'))!!}
                    </div>
                
                    <div class="form-group">
                        <input type="button" onclick = "add('page')" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add To Menu') }}">
                    </div>
                </div>
                <div class="posts">
                    <div class=" form-group">
                        {{ Form::label('posts', 'Posts', array('class' => 'control-label')) }}
                        {!! Form::select('posts[]',$posts,null,array('multiple'=>'multiple','class'=>'form-control','id'=>'posts'))!!}
                    </div>
                
                    <div class="form-group">
                        <input type="button" onclick = "add('post')" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add To Menu') }}">
                    </div>
                </div>
                <div class="categories">
                    <div class=" form-group">
                        {{ Form::label('categories', 'Categories', array('class' => 'control-label')) }}
                        {!! Form::select('categories[]',$categories,null,array('multiple'=>'multiple','class'=>'form-control','id'=>'categories'))!!}
                    </div>
                
                    <div class="form-group">
                        <input type="button" onclick = "add('category')" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add To Menu') }}">
                    </div>
                </div>
                <div class="tag">
                    <div class=" form-group">
                        {{ Form::label('tags', 'Tags', array('class' => 'control-label')) }}
                        {!! Form::select('tags[]',$tags,null,array('multiple'=>'multiple','class'=>'form-control','id'=>'tags'))!!}
                    </div>
                
                    <div class="form-group">
                        <input type="button" onclick = "add('tag')" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add To Menu') }}">
                    </div>
                </div>
                <div class="custom">
                    {{ Form::label('null', 'Custom URL') }}
                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        
                        {{ Form::text('name',null,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
                        
                    </div>
                    <div class="form-group">
                        {{ Form::label('link', 'Link') }}
                        
                        {{ Form::text('link',null,array('id'=>'link','placeholder'=>'','class'=>'form-control')) }}
                        
                    </div>
                    <div class="form-group">
                        <input type="button" onclick = "add_custom_url()" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Add To Menu') }}">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                @if ($menu->menu_items()->latest()->first())
                    @php
                        $c = $menu->menu_items()->latest()->first();
                        $c = $c->id+1;
                    @endphp
                @else
                @php
                    $c = 1;
                @endphp
                @endif
                <input type="hidden" name="count" value="{{ $c }}" />
                <div class="dd" id="nestable">
                    <ol class="dd-list append">
                        @if ($menu->menu_items)
                            {!! adminmenu($menu->menu_items()->where(['parent'=>0])->orderby('menu_order','ASC')->get()) !!}
                        @endif
                    </ol>
                </div>
            </div>
            <div class="col-sm-2">
               
                <textarea style="display:none" id="nestable-output" rows="3" class="form-control"></textarea>
                <input type="button" onclick = "save_menu()" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Save') }}">
            </div>
        </div>
    </section>

@endsection
@push('cssvender')
<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/jquery-ui/jquery-ui.css') }}" />
<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/jquery-ui/jquery-ui.theme.css') }}" />
@endpush
@push('jsvender')
<script src="{{ asset('admin-design/assets/vendor/jquery-nestable/jquery-nestable.js') }}"></script>
<script src="{{ asset('admin-design/assets/vendor/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('admin-design/assets/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js') }}"></script>
<script src="{{ asset('admin-design/assets/vendor/store2/store2.js') }}"></script>
@endpush
@push('adminjs')
    <script>
        (function($) {

'use strict';
    
/*
Update Output
*/
var updateOutput = function (e) {
    var list = e.length ? e : $(e.target),
        output = list.data('output');

    if (window.JSON) {
        output.val(window.JSON.stringify(list.nestable('serialize')));
    } else {
        output.val('JSON browser support required for this demo.');
    }
};

/*
Nestable 1
*/
$('#nestable').nestable({
    group: 1
}).on('change', updateOutput);

/*
Output Initial Serialised Data
*/
$(function() {
    updateOutput($('#nestable').data('output', $('#nestable-output')));
});

}).apply(this, [jQuery]);
function add_custom_url(){
    var i = $('input[name="count"]').val();
    var label = $('#name').val();
    var url =$('#link').val();
    var str = '';
    str +='<li class="dd-item dd3-item" data-url="'+url+'" data-menu_type="custom_url" data-id="'+i+'" data-item_id="" data-item_model = "" data-label="'+label+'" data-css="">';
    str +='<div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="pull-left">'+label+'</div><div class="pull-right">Custom Url | <a href="javascript:void(0)" onclick="edit_menu_item(this)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>| <a href="javascript:void(0)" data-action="removeRow" onclick="deleteItem(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div>';
    str +='<ol class="dd-list">';
    str +='</ol>';
    str +='</li>';	
    $('input[name="count"]').val(i+1);
    $('.append').append(str);
}

function add(type){
    
    var count = $('input[name="count"]').val();
    switch(type){
        case 'page':
        var values = $('#pages').val();
        break;
        case 'post':
         values = $('#posts').val();
         break;
        case 'category':
        values = $('#categories').val();
        break;
        case 'tag':
        values = $('#tags').val();
        break;
    }
    
    var token = "{{ csrf_token()  }}";
    $.ajax({
        url:"{{ route('admin.menus.getdetail') }}",
        type:"POST",
        data:{
            ids:values,
            type:type,
            _token:token
        },
        success:function(res){
            var li_len = $('.dd-list').find('li').length;
            
            var str ='';
            
                var i =count;
                for(x in res){
                    
                    str +='<li class="dd-item dd3-item" data-url="" data-menu_type="'+type+'" data-id="'+i+'" data-item_id="'+res[x].id+'" data-item_model = "'+res[x].model+'"data-label="'+res[x].name+'" data-css="">';
                    str +='<div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="pull-left">'+res[x].name+'</div><div class="pull-right">'+type+' | <a href="javascript:void(0)" onclick="edit_menu_item(this)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>| <a href="javascript:void(0)" data-action="removeRow" onclick="deleteItem(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div>';
                    str +='<ol class="dd-list">';
                    str +='</ol>';
                    str +='</li>';						
					i++;							
                }
            
            
            $('input[name="count"]').val(i);
            $('.append').append(str);
                switch(type){
                    case 'page':
                    $("#pages option:selected").removeAttr("selected");
                    
                    break;
                    case 'post':
                    $("#posts option:selected").removeAttr("selected");
                    break;
                    case 'category':
                    $("#categories option:selected").removeAttr("selected");
                    break;
                    case 'tag':
                    $("#tags option:selected").removeAttr("selected");
                    break;
                }
        }
    })
}


function deleteItem(send){
    var p = $(send).parent().parent().parent();
    p.remove();
    new PNotify({
                    title: 'Menus',
                    text: 'Menu Item deleted successfully!',
                    type: 'error'
                });
}
function save_menu(){
    var menu = $('#nestable').nestable('serialize');
    var token = "{{ csrf_token()  }}";
    $.ajax({
        url:"{{ route('admin.menus.items.save') }}",
        type:"POST",
        dataType:"JSON",
        data:{
            _token:token,
            menu:menu,
            menu_id:{{ $menu->id }}
        },
        success:function(res){
            if(res.status){
                new PNotify({
                    title: 'Menus',
                    text: 'Menu Item save successfully!',
                    type: 'success'
                });
            }else{
                new PNotify({
                    title: 'Menus',
                    text: 'Somethings wrong',
                    type: 'error'
                });
            }
            
        }
    });
    
}

function edit_menu_item(send){
    var el = $(send).parent().parent().parent();
   
   $('#modalForm').find('h2').html('Menu Item: '+$(el).data('label'));
   $('#modalForm').find('#lable').val($(el).data('label'));
   $('#modalForm').find('#css_class').val($(el).data('css'));
   $('#modalForm').find('#li-id').val($(el).data('id'));
   if($(el).data('menu_type')=='custom_url'){
      $('.url-custom').show();
      $('#modalForm').find('#custom_url').val($(el).data('url'));
   }
    $.magnificPopup.open({
        items: {
            src: '#modalForm',
            type: 'inline'
        }
    });
    
}

function save_item(){
    var current = $('#li-id').val();
    var label = $('#lable').val();
    var css = $('#css_class').val();
    var url = $('#custom_url').val();
    var el = $('.dd-list').find("[data-id='" + current + "']");
    $(el).find('.dd3-content').find('.pull-left').html(label);
    $(el).data('label',label);
    $(el).data('css',css);
    $(el).data('url',url);
    $.magnificPopup.close();
    
}
    </script>
    <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">{{ 'Menu Item' }}</h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="li-id" value="" />
                <div class="form-group col-sm-6">
                    {{ Form::label('lable', 'Label') }}
                    
                    {{ Form::text('lable',null,array('id'=>'lable','placeholder'=>'','class'=>'form-control')) }}
                    
                </div>
                <div class="form-group col-sm-6">
                    {{ Form::label('css_class', 'CSS Class') }}
                    
                    {{ Form::text('css_class',null,array('id'=>'css_class','placeholder'=>'','class'=>'form-control')) }}
                    
                </div>
                <div class="form-group col-sm-12 url-custom" style="display: none;">
                    {{ Form::label('custom_url', 'Url') }}
                    
                    {{ Form::text('custom_url',null,array('id'=>'custom_url','placeholder'=>'','class'=>'form-control')) }}
                    
                </div>
                <div class="form-group col-sm-12">
                    <input type="button" onclick="save_item()" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Update') }}">
                </div>
            </div>
        </section>
    </div>
@endpush
