@extends('layouts.master')
@section('title') Edit Menu @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Edit Menu') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.menus') }}">
                    <span>{{ __('Menus') }}</span>
                </a>
            </li>
            <li>
                <span>{{ __('Edit Menu') }}</span>
            </li>
            
        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>
</header>   
<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                
            </div>
    
            <h2 class="panel-title">{{ __('Edit Menu') }}</h2>
        </header>
        <div class="panel-body">
            {!! Form::model($menu, ['method' => 'POST','route' => ['admin.menus.update', $menu->id]]) !!}
                <div class="form-group @error('name') has-error @enderror">
                    {{ Form::label('name', 'Name') }}
                    
                    {{ Form::text('name',null,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
                    @error('name')
                        <label id="name" class="error" >{{ $message }}</label>
                    @enderror 
                </div>
                <div class="form-group">
                    <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Update') }}">
                </div>
            {{ Form::close() }}
        </div>
    </section>
        
</div>
@endsection
