@extends('layouts.master')
@section('title') Edit Admin Users @endsection
@section('content')
<header class="page-header">
    <h2>{{ __('Edit  User') }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.adminusers') }}">
                    <span>{{ __(' Users') }}</span>
                </a>

            </li>
            <li>
                <span>{{ __('Edit') }}</span>
            </li>

        </ol>

        <a class="sidebar-right-toggle" ></a>
    </div>

</header>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>

        </div>

        <h2 class="panel-title">{{ __('Edit  User') }}</h2>
    </header>
    <div class="panel-body">
        {!! Form::model($user, ['method' => 'POST','route' => ['admin.users.update', $user->id]]) !!}
        <div class="form-group @error('name') has-error @enderror">
            {{ Form::label('name', 'Name') }}

            {{ Form::text('name',null,array('id'=>'name','placeholder'=>'','class'=>'form-control')) }}
            @error('name')
                <label id="name" class="error" >{{ $message }}</label>
            @enderror
        </div>
        <div class="form-group @error('email') has-error @enderror">
            {{ Form::label('email', 'Email') }}

            {{ Form::text('email',null,array('id'=>'email','placeholder'=>'','class'=>'form-control')) }}
            @error('email')
                <label id="name" class="error" >{{ $message }}</label>
            @enderror
        </div>
        <div class=" form-group @error('roles') has-error @enderror">
            {{ Form::label('roles', 'Roles', array('class' => 'control-label')) }}
            {!! Form::select('roles[]',$roles,null,array('multiple'=>'multiple','class'=>'form-control','id'=>'roles'))!!}
            @error('roles')
                <label id="roles-error" class="error" >{{ $message }}</label>
            @enderror
        </div>
        <div class="form-group @error('new_password') has-error @enderror">
            {{ Form::label('new_password', 'New Password', array('class' => 'control-label')) }}
            {{ Form::password('new_password',array('id'=>'password','placeholder'=>'','class'=>'form-control')) }}
            @error('new_password')
                <label id="new_password_error" class="error" >{{ $message }}</label>
            @enderror
        </div>
        <div class="form-group @error('conf_password') has-error @enderror">
            {{ Form::label('conf-password', 'Conf Password', array('class' => 'control-label')) }}
            {{ Form::password('conf_password',array('id'=>'conf-password','placeholder'=>'','class'=>'form-control')) }}
            @error('conf_password')
                <label id="conf_password_error" class="error" >{{ $message }}</label>
            @enderror
        </div>
        <div class="form-group">
            <input type="submit" class="mb-xs mt-xs mr-xs btn btn-primary" value="{{ __('Save') }}">
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
{{-- Page Specific CSS Vendor --}}
@push('cssvender')
<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/select2/css/select2.css') }}" />
@endpush
{{-- Page Specific JS Vendor --}}
@push('jsvender')
    <script src="{{ asset('admin-design/assets/vendor/select2/js/select2.js') }}"></script>
@endpush
@push('adminjs')
    @if ($message = Session::get('success'))
        <script>
            new PNotify({
                    title: 'Admin User',
                    text: '{{ $message }}',
                    type: 'success'
                });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            new PNotify({
                    title: 'Admin User',
                    text: '{{ $message }}',
                    type: 'error'
                });
        </script>
    @endif
    <script>
        $("#roles").select2();
    </script>
@endpush
