<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    @php
        $fav_icon = Setting::get('site_favicon_id',true)
    @endphp
    @if($fav_icon)
        @php
            $fav = $fav_icon->media()->first();
        @endphp

    @endif
    @if(isset($fav))
        @php
            $url = $fav->url;

        @endphp

       <link rel="icon" href="{{ $url }}" type="image/gif" sizes="16x16">

    @endif

    <!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />



    <title>@yield('title') | {{ config('app.name') }} </title>
    <!-- Web Fonts  -->
    {!! Html::Style('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light') !!}
    <!-- Vendor CSS -->

    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/bootstrap/css/bootstrap.css') }}" />

	<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/font-awesome/css/font-awesome.css')}}" />
	<link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/magnific-popup/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')}}" />
    <!-- Specific Page Vendor CSS -->
    @stack('cssvender')
    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('admin-design/assets/vendor/pnotify/pnotify.custom.css')}}" />
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('admin-design/assets/stylesheets/theme.css')}}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('admin-design/assets/stylesheets/skins/default.css')}}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('admin-design/assets/stylesheets/theme-custom.css')}}">

    <!-- Head Libs -->
    <script src="{{ asset('admin-design/assets/vendor/modernizr/modernizr.js')}}"></script>
    @stack('admincss')
</head>
<body>
    @php
        $route = Route::currentRouteName()
    @endphp
    @if ($route=='admin.login')
        @yield('content_login')
    @else
    <section class="body">
        @if($route=='admin.media.ckbrowse')
            @yield('content')
        @else
        @include('elements.admin.top-header')
            <div class="inner-wrapper">
                @include('elements.admin.sidebar')
                <section role="main" class="content-body">
                    @yield('content')
                </section>
            </div>
        @endif


    </section>
    @endif

    <!-- Vendor -->
		<script src="{{ asset('admin-design/assets/vendor/jquery/jquery.js')}}"></script>
		<script src="{{ asset('admin-design/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{ asset('admin-design/assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{ asset('admin-design/assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
		<script src="{{ asset('admin-design/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{ asset('admin-design/assets/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
		<script src="{{ asset('admin-design/assets/vendor/jquery-placeholder/jquery-placeholder.js')}}"></script>

        <!-- Specific Page Vendor -->
        @stack('jsvender')
        <script src="{{ asset('admin-design/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
        <script src="{{ asset('admin-design/assets/vendor/pnotify/pnotify.custom.js')}}"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('admin-design/assets/javascripts/theme.js')}}"></script>

		<!-- Theme Custom -->
		<script src="{{ asset('admin-design/assets/javascripts/theme.custom.js')}}"></script>

		<!-- Theme Initialization Files -->
        <script src="{{ asset('admin-design/assets/javascripts/theme.init.js')}}"></script>
        {{-- Modal js --}}
        <script src="{{ asset('admin-design/assets/javascripts/ui-elements/examples.modals.js')}}"></script>
        @stack('adminjs')
        <script>

        function common_upload(send){
            var dname =  $(send).data('push');
            var fd = new FormData();
            var  files = $('input[name="upload-file-'+dname+'"]')[0].files[0];
            var _token = $('input[name="token-'+dname+'"]').val();
            fd.append('file',files);
            fd.append('_token',_token);
            $.ajax({
                url:"{{ route('admin.media.store') }}",
                type:"POST",
                data:fd,
                contentType: false,
                processData: false,
                success:function(res){
                    $('input[name="'+dname+'"').val(res.url);
                    $('input[name="'+dname+'_id"').val(res.id);
                    $('input[name="'+dname+'_alt"').val(res.alt);
                    $('input[name="upload-file-'+dname+'"]').val('');
                    $('.upload-btn-'+dname).html('');
                    $('.upload-btn-'+dname).html('<a class=" btn btn-danger" onclick="remove_url(this)" hrel="#" data-push="'+dname+'"><i class="fa fa-trash" aria-hidden="true"></i></a>');
                    $( ".fileupload-exists" ).trigger( "click" );
                    $.magnificPopup.close();

                }
            });

        }
        function remove_url(send){
            var dname =  $(send).data('push');
            $('input[name="'+dname+'"').val('');
        // $('input[name="'+dname+'_id"').val('');
            $('input[name="'+dname+'_alt"').val('');
            $('.upload-btn-'+dname).html('');
            $('.upload-btn-'+dname).html('<a class="modal-sizes btn btn-success" href="#modal_'+dname+'"><i class="fa fa-upload" aria-hidden="true"></i></a>');
            $('.modal-sizes').magnificPopup({
                type: 'inline',
                preloader: false,
                modal: true
            });

        }



    $(document).on('click','.selectfile',function(e){
        e.preventDefault();
        var dname =  $(this).data('push');
        var url = $(this).data('url');
        var id = $(this).data('id');
        var alt = $(this).data('alt');
        $('input[name="'+dname+'"').val(url);
        $('input[name="'+dname+'_id"').val(id);
        $('input[name="'+dname+'_alt"').val(alt);
        $('.upload-btn-'+dname).html('');
        $('#select-'+dname).removeClass('active');
        $('#li-nav-select-'+dname).removeClass('active');
        $('#li-nav-form-'+dname).addClass('active');
        $('#upload-'+dname).addClass('active');
        $('#popupload').html('');
        $('#page-no-'+dname).val(1);
        $('.upload-btn-'+dname).html('<a class=" btn btn-danger" onclick="remove_url(this)" hrel="#" data-push="'+dname+'"><i class="fa fa-trash" aria-hidden="true"></i></a>');
        $.magnificPopup.close();

    });
    function load_image(send){
        var _token = "{{ csrf_token()  }}";
        var name = $(send).data('elename');
        $.ajax({
            url:"{{ route('admin.media') }}",
            type:"GET",
            data:{
                'name':name,
                '_token':_token
            },
            beforesend:function(){
                console.log('loading......');
            },
            success:function(res){
                $('#popupload').html(res);
                console.log(res);
            }
        })
    }
        </script>

<script type="text/javascript">


    $(document).on('click','.loadmore',function(){
        var name = $(this).data('name');
        var page = $('#page-no-'+name).val()
        page++;
        loadMoreData(page,name);
        
    });

	function loadMoreData(page,name){
	  $.ajax(
	        {
	            url: '{{ route('admin.media') }}'+'?page=' + page +'&name='+name,
	            type: "get",
                
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html === ""){
                    
	                $('.ajax-load').html("No more records found");
	                return false;
	            }
                $('.ajax-load').hide();
                $('.ajax-load').html('');
                $('#page-no-'+name).val(page);
	            $("#popupload").append(data.html);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
</script>
</body>
</html>
