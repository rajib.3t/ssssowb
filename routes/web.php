<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * All Admin Group Route 
*/
Route::group(['as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Admin'],  function() {
    // Admin Login and Logout
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login'); 
    Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
    Route::post('/logout', 'Auth\LoginController@adminLogout')->name('logout');
    // End 
    // Admin Dashboard
    Route::get('/', 'AdminController@index')->name('dashboard');
    // End 
    // Admin Profile
    Route::match(['get', 'patch'], '/profile','AdminController@profile')->name('profile');
    // End
    //Admin Setting 
    Route::get('setting/','SettingController@general')->name('setting');
    Route::get('setting/general/','SettingController@general')->name('setting.general');
    Route::get('setting/under-construction','SettingController@underConstruction')->name('setting.underconstruction');
    Route::post('setting/save/general','SettingController@save')->name('setting.general.save');
    Route::post('setting/save/underconstruction','SettingController@save')->name('setting.underconstruction.save');
    Route::get('setting/miscellaneous','SettingController@miscellaneous')->name('setting.miscellaneous');
    Route::post('setting/save/miscellaneous','SettingController@save')->name('setting.miscellaneous.save');
    //End

    // Media 
    Route::get('media/','MediaController@index')->name('media');
    Route::get('media/create','MediaController@create')->name('media.create');
    Route::post('media/store','MediaController@store')->name('media.store');
    Route::post('media/ckeditor','MediaController@ckditor_store')->name('media.ckeditor');
    Route::get('media/ckbrowse','MediaController@ckbrowse')->name('media.ckbrowse');
    Route::get('media/delete/{id}','MediaController@destroy')->name('media.delete');
    Route::post('media/delete-all','MediaController@deleteall')->name('media.delete.all');
    // end

    // Pages 
    Route::get('pages/','PageController@index')->name('pages');
    Route::get('page/create','PageController@create')->name('pages.create');
    Route::post('page/save','PageController@store')->name('pages.save');
    Route::get('page/edit/{id}','PageController@edit')->name('pages.edit');
    Route::post('page/update/{id}','PageController@update')->name('pages.update');
    Route::get('page/delete/{id}','PageController@destroy')->name('pages.delete');
    // End
    /**
     * Posts
     */
    Route::get('posts/','PostController@index')->name('posts');
    Route::get('post/create','PostController@create')->name('posts.create');
    Route::post('post/save','PostController@store')->name('posts.save');
    Route::get('post/edit/{id}','PostController@edit')->name('posts.edit');
    Route::post('post/update/{id}','PostController@update')->name('posts.update');
    Route::get('post/delete/{id}','PostController@destroy')->name('posts.delete');
    /**
     * End 
     */
    /**
     * Category
     */
    Route::get('categories/','CategoryController@index')->name('categories');
    Route::post('category/save','CategoryController@store')->name('categories.save');
    Route::get('category/edit/{id}','CategoryController@edit')->name('categories.edit');
    Route::post('category/update/{id}','CategoryController@update')->name('categories.update');
    Route::get('category/delete/{id}','CategoryController@destroy')->name('categories.delete');
    /**
     * End 
     */
    /**
     * Tags
     */
    Route::get('tags/','TagController@index')->name('tags');
    Route::post('tag/save','TagController@store')->name('tags.save');
    Route::get('tag/delete/{id}','TagController@destroy')->name('tags.delete');
    Route::get('tag/status/{id}','TagController@status')->name('tags.status');
    Route::post('tag/ajax','TagController@ajax')->name('tags.ajax');
    /**
     * End 
     */
    /**
     * Menus
     */
    Route::get('menus/','MenuController@index')->name('menus');
    Route::post('menu/save','MenuController@store')->name('menus.save');
    Route::get('menu/status/{id}','MenuController@status')->name('menus.status');
    Route::get('menu/edit/{id}','MenuController@edit')->name('menus.edit');
    Route::post('menu/update/{id}','MenuController@update')->name('menus.update');
    Route::get('menu/add-items/{id}','MenuController@addItem')->name('menus.additems');
    Route::post('menu/getdetails','MenuController@getMenuDetails')->name('menus.getdetail');
    Route::post('menu/items/save','MenuController@saveMenuItems')->name('menus.items.save');
    Route::get('menu/delete/{id}','MenuController@destroy')->name('menus.delete');
    /**
     * End 
     */

    /**
     * Admin User
     */
    Route::get('admin-users','AdminUsers@index')->name('adminusers');
    Route::get('admin-user/create','AdminUsers@create')->name('adminuser.create');
    Route::post('admin-user/save','AdminUsers@store')->name('adminuser.store');
    Route::get('admin-user/edit/{id}','AdminUsers@edit')->name('adminuser.edit');
    Route::post('admin-user/update/{id}','AdminUsers@update')->name('adminuser.update');
    Route::post('admin-user/special-permission/{id}','AdminUsers@specialPermission')->name('adminuser.special.permission');
    /**
     * End
     */
    /**
     * User
     */
    Route::get('users','UserController@index')->name('users');
    Route::get('user/create','UserController@create')->name('users.create');
    Route::post('user/store','UserController@store')->name('users.save');
    Route::get('user/edit/{id}','UserController@edit')->name('users.edit');
    Route::post('user/update/{id}','UserController@update')->name('users.update');
    /**
     * End
     */
    /**
     * Role
     */
    Route::get('roles','RoleController@index')->name('roles');
    Route::post('role/save','RoleController@store')->name('role.save');
    Route::get('role/edit/{id}','RoleController@edit')->name('role.edit');
    Route::post('role/update/{id}','RoleController@update')->name('role.update');
    Route::get('role/delete/{id}','RoleController@destroy')->name('role.delete');
    Route::post('role-add-to-permissions/{id}','RoleController@addRoleToPermissions')->name('roleToPermissions');
    /**
     * End 
     */
    /**
     * Permissions
     */
    Route::get('permissions','PermissionController@index')->name('permissions');
    Route::post('permission/save','PermissionController@store')->name('permission.save');
    Route::get('permission/edit/{id}','PermissionController@edit')->name('permission.edit');
    Route::post('permission/update/{id}','PermissionController@update')->name('permission.update');
    Route::get('permission/delete/{id}','PermissionController@destroy')->name('permission.delete');
    Route::post('permission-add-to-roles/{id}','PermissionController@addPermissionToRoles')->name('permissionToroles');
});
/**
 * End of Admin route
 */

// Route::get('/', function () {
//    dd();
// });
Route::get('/', 'PageController@index')->name('home-page');
// Route::get('/test', function () {
    
//     $a = Auth::guard('admin')->user();
//     $r = Userdata::get($a,'test');
//     dd($r);
// });
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
